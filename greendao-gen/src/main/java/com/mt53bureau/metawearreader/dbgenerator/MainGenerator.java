package com.mt53bureau.metawearreader.dbgenerator;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;
import org.greenrobot.greendao.generator.ToMany;

import java.io.File;

public class MainGenerator {

    private static final String slash = File.separator;
    private static final String OUTPUT_PATH = System.getProperty("user.dir") + slash + "app" + slash + "src" + slash + "main" + slash + "java";

    public static void main(String[] args) throws Exception {
        final Schema schema = new Schema(3, "com.mt53bureau.metawearreader.db");
        schema.enableKeepSectionsByDefault();
        addTables(schema);
        new DaoGenerator().generateAll(schema, OUTPUT_PATH);
    }

    private static void addTables(Schema schema) {
        final Entity session = addSessionsTable(schema);
        final Entity reading = addBoardReadingsTable(schema);
        final Property sessionId = reading.addLongProperty("sessionId").notNull().getProperty();
        final ToMany sessionToReadings = session.addToMany(reading, sessionId);
        sessionToReadings.setName("readings");
    }

    private static Entity addSessionsTable(final Schema schema) {
        final Entity entity = schema.addEntity("CaptureSession");
        entity.addIdProperty();
        entity.addDateProperty("start").notNull();
        entity.addDateProperty("end");
        return entity;
    }

    private static Entity addBoardReadingsTable(final Schema schema) {
        final Entity entity = schema.addEntity("BoardReading");
        entity.addIdProperty();
        entity.addStringProperty("boardAddress").notNull();
        entity.addDateProperty("timestamp").notNull();
        entity.addFloatProperty("accX");
        entity.addFloatProperty("accY");
        entity.addFloatProperty("accZ");
        entity.addFloatProperty("gyrX");
        entity.addFloatProperty("gyrY");
        entity.addFloatProperty("gyrZ");
        entity.addFloatProperty("magX");
        entity.addFloatProperty("magY");
        entity.addFloatProperty("magZ");
        return entity;
    }
}
