package com.mt53bureau.metawearreader.events;

public class BoardDisconnectedEvent extends DeviceStateChangedReport {

    public BoardDisconnectedEvent(final String deviceAddress) {
        super(deviceAddress);
    }
}
