package com.mt53bureau.metawearreader.events;

public class BoardConnectedEvent extends DeviceStateChangedReport {

    public BoardConnectedEvent(final String deviceAddress) {
        super(deviceAddress);
    }
}
