package com.mt53bureau.metawearreader.events;

/**
 * Notifies about any change about state of connected device
 */
public class DeviceStateChangedReport {
    private final String mDeviceAddress;


    public DeviceStateChangedReport(final String deviceAddress) {
        mDeviceAddress = deviceAddress;
    }

    public String getDeviceAddress() {
        return mDeviceAddress;
    }
}
