package com.mt53bureau.metawearreader.events;

public class BoardRemovedFromList extends DeviceStateChangedReport {

    public BoardRemovedFromList(final String deviceAddress) {
        super(deviceAddress);
    }
}
