package com.mt53bureau.metawearreader.events;

import com.mt53bureau.metawearreader.enums.ConnectionStatus;

public class ConnectionStateChangedReport extends DeviceStateChangedReport {
    private final ConnectionStatus mConnectionStatus;

    public ConnectionStateChangedReport(final String deviceAddress, ConnectionStatus connectionStatus) {
        super(deviceAddress);
        mConnectionStatus = connectionStatus;
    }

    public ConnectionStatus getConnectionStatus() {
        return mConnectionStatus;
    }
}
