package com.mt53bureau.metawearreader.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class BtDeviceInfo implements Parcelable {
    private final String bluetoothAddress;
    private final String bluetoothName;

    public BtDeviceInfo(final String address, final String name) {
        if (TextUtils.isEmpty(address)) {
            throw new IllegalStateException("Bluetooth address can not be empty");
        }
        this.bluetoothAddress = address;
        this.bluetoothName = name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final BtDeviceInfo that = (BtDeviceInfo) o;

        return bluetoothAddress.equals(that.bluetoothAddress);

    }

    @Override
    public int hashCode() {
        return bluetoothAddress.hashCode();
    }

    public final String getBluetoothAddress() {
        return bluetoothAddress;
    }

    public final String getBluetoothName() {
        return bluetoothName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bluetoothAddress);
        dest.writeString(this.bluetoothName);
    }

    protected BtDeviceInfo(Parcel in) {
        this.bluetoothAddress = in.readString();
        this.bluetoothName = in.readString();
    }

    public static final Creator<BtDeviceInfo> CREATOR = new Creator<BtDeviceInfo>() {
        @Override
        public BtDeviceInfo createFromParcel(Parcel source) {
            return new BtDeviceInfo(source);
        }

        @Override
        public BtDeviceInfo[] newArray(int size) {
            return new BtDeviceInfo[size];
        }
    };
}
