package com.mt53bureau.metawearreader.model;

import com.mt53bureau.metawearreader.CsvFormatters;

import java.util.Calendar;
import java.util.Locale;

public class ModuleReading {

    public final Calendar timestamp;
    public final float x;
    public final float y;
    public final float z;

    public ModuleReading(final Calendar timestamp, float x, float y, float z) {
        this.timestamp = timestamp;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static String toCsv(ModuleReading reading) {
        if (reading != null) {
            return String.format(Locale.US, "%s,%s,%s", CsvFormatters.FLOAT_FORMATTER.format(reading.x),
                    CsvFormatters.FLOAT_FORMATTER.format(reading.y), CsvFormatters.FLOAT_FORMATTER.format(reading.z));
        } else {
            return ",,";
        }
    }
}
