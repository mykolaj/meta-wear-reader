package com.mt53bureau.metawearreader.model;

import java.util.Calendar;

import static com.mt53bureau.metawearreader.CsvFormatters.DATE_FORMATTER;
import static com.mt53bureau.metawearreader.CsvFormatters.MILLIS_FORMATTER;
import static com.mt53bureau.metawearreader.CsvFormatters.TIME_FORMATTER;

public class ModuleReadingWrapper {

    public final Calendar timestamp;
    public final ModuleReading accelerometerReading;
    public final ModuleReading gyroscopeReading;
    public final ModuleReading magnetometerReading;

    public ModuleReadingWrapper(final Calendar timestamp, final ModuleReading accelerometerReading, final ModuleReading gyroscopeReading, final ModuleReading magnetometerReading) {
        this.timestamp = timestamp;
        this.accelerometerReading = accelerometerReading;
        this.gyroscopeReading = gyroscopeReading;
        this.magnetometerReading = magnetometerReading;
    }

    @Override
    public String toString() {
        final long millis = timestamp.getTimeInMillis();
        return DATE_FORMATTER.print(millis) + ',' +
                TIME_FORMATTER.print(millis) + ',' +
                MILLIS_FORMATTER.format(millis) + ',' +
                ModuleReading.toCsv(accelerometerReading) + ',' +
                ModuleReading.toCsv(gyroscopeReading) + ',' +
                ModuleReading.toCsv(magnetometerReading);
    }
}
