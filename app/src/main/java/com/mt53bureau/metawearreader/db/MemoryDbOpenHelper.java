package com.mt53bureau.metawearreader.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.greenrobot.greendao.database.Database;


public class MemoryDbOpenHelper extends DaoMaster.OpenHelper {

    public MemoryDbOpenHelper(Context context) {
        super(context, null);
    }

    public MemoryDbOpenHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, null, factory);
    }

    @Override
    public void onCreate(final Database db) {
        BoardReadingDao.createTable(db, true);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        Log.i("greenDAO", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");
        dropAllTables(db, true);
        onCreate(db);
    }

    /** Drops underlying database table using DAOs. */
    public static void dropAllTables(Database db, boolean ifExists) {
        BoardReadingDao.dropTable(db, ifExists);
    }
}
