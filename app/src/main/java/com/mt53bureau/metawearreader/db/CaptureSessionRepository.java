package com.mt53bureau.metawearreader.db;

import android.content.Context;

import com.mt53bureau.metawearreader.CurrentApplication;

import org.joda.time.DateTime;

public final class CaptureSessionRepository {
    private CaptureSessionRepository() {
    }

    public static CaptureSession createSession(Context context) {
        if (context == null) {
            return null;
        }
        final CaptureSession session = new CaptureSession();
        session.setStart(DateTime.now().toDate());
        final CaptureSessionDao table = getSessionsTable(context);
        final long rowId = table.insert(session);
//        table.updateKeyAfterInsert(session, rowId);
        return session;
    }

    public static void updateSession(Context context, CaptureSession session) {
        if (context == null || session == null) {
            return ;
        }

        getSessionsTable(context).update(session);
    }

    private static CaptureSessionDao getSessionsTable(final Context context) {
        return ((CurrentApplication) context.getApplicationContext()).getDiskDaoSession().getCaptureSessionDao();
    }
}
