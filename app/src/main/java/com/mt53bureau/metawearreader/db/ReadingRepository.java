package com.mt53bureau.metawearreader.db;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.mt53bureau.metawearreader.CurrentApplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ReadingRepository {

    public static final String TRIGGER_BOARD_RECORD_DELETE = "board_record_delete";
    private static final String VIEW_COLUMNS = BoardReadingDao.Properties.Id + ", " +
            BoardReadingDao.Properties.BoardAddress + ", " +
            BoardReadingDao.Properties.Timestamp + ", " +
            BoardReadingDao.Properties.AccX + ", " +
            BoardReadingDao.Properties.AccY + ", " +
            BoardReadingDao.Properties.AccZ + ", " +
            BoardReadingDao.Properties.GyrX + ", " +
            BoardReadingDao.Properties.GyrY + ", " +
            BoardReadingDao.Properties.GyrZ + ", " +
            BoardReadingDao.Properties.MagX + ", " +
            BoardReadingDao.Properties.MagY + ", " +
            BoardReadingDao.Properties.MagZ + ", " +
            BoardReadingDao.Properties.SessionId;

    private ReadingRepository() {
    }

    private static BoardReadingDao getBoardReadingTable(final Context context) {
        // This table is stored inside an in-memory database
        return ((CurrentApplication) context.getApplicationContext()).getMemoryDaoSession().getBoardReadingDao();
    }

    public static long insertReading(Context context, BoardReading reading) {
        if (context == null) {
            return -1;
        }
        final BoardReadingDao table = getBoardReadingTable(context);
        final long id = table.insert(reading);
        table.updateKeyAfterInsert(reading, id);
        return id;
    }

    @NonNull
    public static List<String> getExistingBoards(final Context context) {
        if (context == null) {
            return Collections.emptyList();
        }
        final String board_reading = BoardReadingDao.TABLENAME;
        final String board_address = BoardReadingDao.Properties.BoardAddress.columnName;
        final String query = "select " + board_address + " from " + board_reading + " group by "
                + board_address + ";";
        final Cursor cursor = getBoardReadingTable(context).getDatabase().rawQuery(query, null);

        if (cursor == null) {
            return Collections.emptyList();
        }
        if (!cursor.moveToFirst()) {
            cursor.close();
        }
        final List<String> addresses = new ArrayList<>();
        while (cursor.moveToNext()) {
            final String addr = cursor.getString(cursor.getColumnIndex(board_address));
            addresses.add(addr);
        }
        cursor.close();
        return addresses;
    }

    /**
     * Creates SQL view which contains records only for given board address.
     * The view is named with the name which matches a board address
     * @param context
     * @param boardAddress
     */
    public static void createViewIfNotExists(final Context context, final String boardAddress) {
        if (context == null || TextUtils.isEmpty(boardAddress)) {
            return;
        }
        final String board_reading = BoardReadingDao.TABLENAME;
        final String id = BoardReadingDao.Properties.Id.columnName;
        final String board_address = BoardReadingDao.Properties.BoardAddress.columnName;
        final String timestamp = BoardReadingDao.Properties.Timestamp.columnName;

        final String createView =
                "create temp view if not exists '" + boardAddress + "'"
                + " as select " + VIEW_COLUMNS + " from " + board_reading
                + " where " + board_address + " = '" + boardAddress + "'"
                + " order by " + timestamp + " asc;";
        final String createTrigger =
                "create trigger if not exists '" + TRIGGER_BOARD_RECORD_DELETE + "'"
                + " instead of delete on '" + boardAddress + "'"
                + " begin "
                + "    delete from " + board_reading + " where " + id + " = OLD." + id + ";"
                + " end;";
        getBoardReadingTable(context).getDatabase().execSQL(createView);
        getBoardReadingTable(context).getDatabase().execSQL(createTrigger);
    }

    public static void deleteRecord(Context context, BoardReading boardReading) {
        if (context == null || boardReading == null) {
            return;
        }
        getBoardReadingTable(context).delete(boardReading);
    }

    @Nullable
    public static Cursor openView(final Context context, final String sqlViewName) {
        if (context == null || TextUtils.isEmpty(sqlViewName)) {
            return null;
        }
        final String timestamp = BoardReadingDao.Properties.Timestamp.columnName;
        final String query = "select * from '" + sqlViewName + "'" +
                " order by " + timestamp + " asc;";
        return getBoardReadingTable(context).getDatabase().rawQuery(query, null);
    }

    public static Cursor selectAllTimestamps(final Context context) {
        if (context == null) {
            return null;
        }
        final String board_reading = BoardReadingDao.TABLENAME;
        final String timestamp = BoardReadingDao.Properties.Timestamp.columnName;
        final String query = "select " + timestamp + " from " + board_reading + " order by "
                + timestamp + " asc;";
        return getBoardReadingTable(context).getDatabase().rawQuery(query, null);
    }

    public static List<BoardReading> getReadings(final Context context, final long sessionId) {
        if (context == null) {
            return Collections.emptyList();
        }
        return getBoardReadingTable(context).queryBuilder().listLazy();
    }
}
