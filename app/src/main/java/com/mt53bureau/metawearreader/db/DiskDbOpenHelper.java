package com.mt53bureau.metawearreader.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.greenrobot.greendao.database.Database;


public class DiskDbOpenHelper extends DaoMaster.OpenHelper {

    public DiskDbOpenHelper(Context context, String name) {
        super(context, name);
    }

    public DiskDbOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onCreate(final Database db) {
        CaptureSessionDao.createTable(db, true);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        Log.i("greenDAO", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");
        dropAllTables(db, true);
        onCreate(db);
    }

    /** Drops underlying database table using DAOs. */
    public static void dropAllTables(Database db, boolean ifExists) {
        CaptureSessionDao.dropTable(db, ifExists);
    }
}
