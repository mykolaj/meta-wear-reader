package com.mt53bureau.metawearreader.enums;

public enum ConnectionStatus {
    CONNECTED,
    DISCONNECTED,
    UNKNOWN
}

