package com.mt53bureau.metawearreader;


import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public final class CsvFormatters {
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.shortDate().withLocale(Locale.US);
    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormat.forPattern("HH:mm:ss.SSS").withLocale(Locale.US);
    public static final DecimalFormat MILLIS_FORMATTER = new DecimalFormat("#");
    public static final NumberFormat FLOAT_FORMATTER = NumberFormat.getInstance(Locale.US);

    static {
        CsvFormatters.FLOAT_FORMATTER.setMaximumFractionDigits(3);
    }

    private CsvFormatters() {
    }
}
