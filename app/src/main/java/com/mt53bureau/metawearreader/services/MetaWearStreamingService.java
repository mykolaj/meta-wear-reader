package com.mt53bureau.metawearreader.services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.mbientlab.metawear.AsyncOperation;
import com.mbientlab.metawear.MetaWearBleService;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.module.Accelerometer;
import com.mbientlab.metawear.module.Bmm150Magnetometer;
import com.mbientlab.metawear.module.Gyro;
import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.CsvExporter;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.datahandle.BoardConnectionHandler;
import com.mt53bureau.metawearreader.db.BoardReading;
import com.mt53bureau.metawearreader.db.CaptureSession;
import com.mt53bureau.metawearreader.db.CaptureSessionRepository;
import com.mt53bureau.metawearreader.enums.ConnectionStatus;
import com.mt53bureau.metawearreader.events.BatteryUpdateEvent;
import com.mt53bureau.metawearreader.events.BoardAddedToListEvent;
import com.mt53bureau.metawearreader.events.BoardConnectedEvent;
import com.mt53bureau.metawearreader.events.BoardDisconnectedEvent;
import com.mt53bureau.metawearreader.events.BoardRemovedFromList;
import com.mt53bureau.metawearreader.events.ConnectionStateChangedReport;
import com.mt53bureau.metawearreader.events.DataRecordingStartedEvent;
import com.mt53bureau.metawearreader.events.DataRecordingStoppedEvent;
import com.mt53bureau.metawearreader.settings.PrefHelper;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.greenrobot.eventbus.EventBus;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;


public class MetaWearStreamingService extends Service {

    public static final String ACCELEROMETER_STREAM_KEY = "accelerometer_stream";
    public static final String GYROSCOPE_STREAM_KEY = "gyroscope_stream";
    public static final String MAGNETOMETER_STREAM_KEY = "magnetometer_stream";
    private static final String LOG_TAG = BuildConfig.LOG_TAG + "." + MetaWearStreamingService.class.getSimpleName();
    private static final long BOARD_RECONNECT_TIMEOUT_MILLIS = 1500;
    private static final Class<Accelerometer> ACCELEROMETER_CLASS = Accelerometer.class /* Bmi160Accelerometer.class */;
    private static final String NO_ACCELEROMETER_MESSAGE = "failed to find " + ACCELEROMETER_CLASS.getSimpleName() + " module. Does this board has such module installed?";
    private static final Class<Gyro> GYROSCOPE_CLASS = Gyro.class /* Bmi160Gyro.class */;
    private static final String NO_GYROSCOPE_MESSAGE = "failed to find " + GYROSCOPE_CLASS.getSimpleName() + " module. Does this board has such module installed?";
    private static final Class<Bmm150Magnetometer> MAGNETOMETER_CLASS = Bmm150Magnetometer.class;
    private static final String NO_MAGNETOMETER_MESSAGE = "failed to find " + MAGNETOMETER_CLASS.getSimpleName() + " module. Does this board has such module installed?";
    private final MetaWearReaderServiceBinder binder = new MetaWearReaderServiceBinder();
    private final MetaWearServiceConnection metaWearServiceConnection = new MetaWearServiceConnection();

    /**
     * For the UI purposes
     */
    private final Map<String, BoardStateInfo> connectedBoardsInfo = new ConcurrentHashMap<>();

    private final Map<String, Integer> deviceToSlotMapping = new ConcurrentHashMap<>();

    private final Map<String, MetaWearBoard> selectedBoards = new ConcurrentHashMap<>();
    private final Map<String, BluetoothDevice> devices = new ConcurrentHashMap<>();

    private final Map<String, BoardConnectionHandler> boardConnectionHandlers = new ConcurrentHashMap<>();

    private ConcurrentMap<String, BlockingDeque<BoardReading>> boardRecordsMap;

    /** value of a single time fraction in millis */
    private int timeSpanMillis;

    /** A boundaries of a time fractions for a current frequency */
    private int[] timeSpans;

    private MetaWearBleService.LocalBinder metaWearServiceBinder;
    private final AtomicBoolean isDataRecordingInProgress = new AtomicBoolean(false);
    private BluetoothAdapter btAdapter;
    private Handler handler;
    private CaptureSession captureSession;

    /** Thread Handler to handle a database input/output in a background */
    private Handler databaseIoHandler;

    /** Thread Handler to handle a CSV file input/output in a background */
    private Handler csvIoHandler;

    /**
     * Thread Handler to handle a task posting
     *
     * So far the UI becomes unresponsive after 10-20 minutes of work. Also i don't know exactly
     * if that's the case, and just trying to test to post new tasks from a special dedicated
     * thread instead of the UI thread, and see what happens.
     */
    private Handler taskPosterHandler;

    private CsvExporter csvExporter;
    private PowerManager.WakeLock wakeLock;
    private RecordConsumerThread recordConsumerThread;

    public static void start(Context context) {
        Intent intent = new Intent(context, MetaWearStreamingService.class);
        context.startService(intent);
    }

    public static void stop(final Context context) {
        context.stopService(new Intent(context, MetaWearStreamingService.class));
    }

    /**
     * @return Bluetooth addresses of currently selected boards
     */
    public Collection<String> getSelectedBoards() {
        return selectedBoards.keySet();
    }

    public Collection<BoardStateInfo> getConnectedBoardsInfo() {
        return new ArrayList<>(connectedBoardsInfo.values());
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        bindService(new Intent(this, MetaWearBleService.class), metaWearServiceConnection, BIND_AUTO_CREATE);
        btAdapter = ((BluetoothManager) getSystemService(BLUETOOTH_SERVICE)).getAdapter();
        HandlerThread databaseIoThread = new HandlerThread("db-io-thread");
        databaseIoThread.start();
        databaseIoHandler = new Handler(databaseIoThread.getLooper());
        HandlerThread csvIoThread = new HandlerThread("csv-io-thread");
        csvIoThread.start();
        csvIoHandler = new Handler(csvIoThread.getLooper());
        HandlerThread taskPosterThread = new HandlerThread("task-poster-thread");
        taskPosterThread.start();
        taskPosterHandler = new Handler(taskPosterThread.getLooper());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(metaWearServiceConnection);
        if (databaseIoHandler != null) {
            databaseIoHandler.getLooper().quit();
        }
        if (csvIoHandler != null) {
            csvIoHandler.getLooper().quit();
        }
        if (taskPosterHandler != null) {
            taskPosterHandler.getLooper().quit();
        }
        if (wakeLock != null) {
            try {
                wakeLock.release();
            } catch (Exception e) {
                Trace.w(LOG_TAG + ": " + ExceptionUtils.getMessage(e));
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Trace.w(LOG_TAG + "onLowMemory: TODO Immediately flush an in-memory data into a CSV file");
        // TODO Immediately flush an in-memory data into a CSV file
    }


    /**
     * Add a device to a list of selected devices, and tries to establish a connection to it right away
     *
     * @param address
     */
    public void selectDevice(final String address) {
        if (metaWearServiceBinder == null) {
            Trace.d(LOG_TAG + ": somehow " + MetaWearBleService.class.getSimpleName()
                    + " is not bound.");
            return;
        }

        if (isDataRecordingInProgress.get()) {
            return;
        }

        if (TextUtils.isEmpty(address)) {
            return;
        }

        final BluetoothDevice device = btAdapter.getRemoteDevice(address);
        final MetaWearBoard board = metaWearServiceBinder.getMetaWearBoard(device);
        if (board == null) {
            Trace.e(LOG_TAG + ": " + MetaWearBleService.class.getSimpleName() + " returned null" +
                    " for a bluetooth device with address \'" + device.getAddress() + "\'.");
            return;
        }

        // Get slot number
        final int number = getFreeSlot(address);

        if (!selectedBoards.containsKey(board.getMacAddress())) {
            selectedBoards.put(board.getMacAddress(), board);
            devices.put(device.getAddress(), device);

            // Notify that a new device were added to a list of selected devices
            EventBus.getDefault().post(new BoardAddedToListEvent(address, number));

            connectBoard(board, false);
        }

        if (!deviceToSlotMapping.containsKey(address)) {
            deviceToSlotMapping.put(address, number);
        }
    }

    /**
     * Remove a device from a selected devices list, and disconnects from it if any connection
     * is established.
     *
     * @param address
     */
    public void unselectDevice(final String address) {
        if (TextUtils.isEmpty(address)) {
            return;
        }

        if (isDataRecordingInProgress.get()) {
            return;
        }

        final MetaWearBoard board = selectedBoards.remove(address);
        deviceToSlotMapping.remove(address);

        if (board != null) {
            disconnectBoard(board);
        }

        // Notify that a device is removed from a list of selected devices
        EventBus.getDefault().post(new BoardRemovedFromList(address));
    }

    /**
     * Start collecting a data from all devices which are in a selected devices list
     */
    public void startDataRecording(@Nullable final String fileName) {
        if (!isDataRecordingInProgress.get()) {
            isDataRecordingInProgress.set(true);
        } else {
            return;
        }

        // Prevent CPU from sleeping while capturing is active
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                String.format("%s-WakeLock", BuildConfig.APPLICATION_ID));
        wakeLock.acquire();

        // Prepare where to store records
        boardRecordsMap = new ConcurrentHashMap<>(selectedBoards.size());
        for (MetaWearBoard b : selectedBoards.values()) {
            boardRecordsMap.put(b.getMacAddress(), new LinkedBlockingDeque<BoardReading>());
        }

        final float accelerometerOutputDataRate = PrefHelper.getAccelerometerOutputDataRate(this).frequency();
        final float gyroscopeOutputDataRate = PrefHelper.getGyroscopeOutputDataRate(this);

        // Calculate time span for a selected frequency

        final float dataFrequency = accelerometerOutputDataRate > gyroscopeOutputDataRate
                ? accelerometerOutputDataRate
                : gyroscopeOutputDataRate;
        timeSpanMillis = Math.round(1000.0f / dataFrequency);
        timeSpans = calculateTimeSpans(timeSpanMillis, dataFrequency);

        // Go into a background thread to
        new Thread(new Runnable() {
            @Override
            public void run() {
                captureSession = CaptureSessionRepository.createSession(MetaWearStreamingService.this);
                final long sessionId = captureSession.getId();
                csvExporter = new CsvExporter(sessionId, fileName, csvIoHandler, deviceToSlotMapping);
                csvExporter.prepare(MetaWearStreamingService.this, devices);

                // Return from background
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        recordConsumerThread = new RecordConsumerThread(sessionId, boardRecordsMap, timeSpanMillis, new RecordConsumerThread.ProcessCallback() {
                            @Override
                            public void onRecordMeasured(final BoardRecordBundle bundle) {
                                processNewReading(bundle);
                            }
                        });
                        recordConsumerThread.start();

                        for (final MetaWearBoard board : new ArrayList<>(selectedBoards.values())) {
                            doBoardSensorsConnect(board);
                        }

                        EventBus.getDefault().post(new DataRecordingStartedEvent());
                    }
                });
            }
        }).start();
    }

    /**
     * Drop all connection to all devices
     */
    public void stopDataRecording() {
        if (!isDataRecordingInProgress.get()) {
            Trace.w(LOG_TAG + ": attempt to stop data recording when there isn't any recording in progress");
            return;
        }

        // Cleanup a capture session
        final CaptureSession session = captureSession;
        captureSession = null;

        // Go into a background thread to update a session database record
        new Thread(new Runnable() {
            @Override
            public void run() {
                session.setEnd(DateTime.now().toDate());
                CaptureSessionRepository.updateSession(MetaWearStreamingService.this, session);

                // Stop thread making a time span calculations
                if (recordConsumerThread != null) {
                    recordConsumerThread.shouldStop();
                    recordConsumerThread = null;
                }

                // Go back to main thread
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        // Disconnect from a board's modules but leave the board connected
                        for (BoardConnectionHandler connectionHandler : new ArrayList<>(boardConnectionHandlers.values())) {
                            connectionHandler.stopAccelerometerStreaming();
                            connectionHandler.stopGyroscopeStreaming();
                            connectionHandler.stopMagnetometerStreaming();
                            connectionHandler.getBoard().tearDown();
                        }

                        // Stop CSV writer
                        if (csvExporter != null) {
                            csvExporter.shouldClose();
                            csvExporter = null;
                        }

                        // Clear any not processed messages left from this stopped session
                        if (csvIoHandler != null) {
                            csvIoHandler.removeCallbacksAndMessages(null);
                        }

                        // Clear any not processed messages left from this stopped session
                        if (databaseIoHandler != null) {
                            databaseIoHandler.removeCallbacksAndMessages(null);
                        }

                        // Clear any not processed messages left from this stopped session
                        if (taskPosterHandler != null) {
                            taskPosterHandler.removeCallbacksAndMessages(null);
                        }

                        if (boardRecordsMap != null) {
                            boardRecordsMap.clear();
                            boardRecordsMap = null;
                        }

                        isDataRecordingInProgress.set(false);
                        EventBus.getDefault().post(new DataRecordingStoppedEvent());

                        // Release a WakeLock to allow a device to go into a sleep mode
                        if (wakeLock != null) {
                            try {
                                wakeLock.release();
                            } catch (Exception e) {
                                Trace.w(LOG_TAG + ": " + ExceptionUtils.getMessage(e));
                            }
                        }
                    }
                });
            }
        }).start();
    }

    /**
     * Connects to a given board and reads its battery state.
     * This function starts to collect a data from the board if the data collection is currently enabled.
     * If the data collection is disabled then this function just establishes the connection and
     * subscribes for a battery state readings.
     *
     * @param board
     */
    private void connectBoard(final MetaWearBoard board, final boolean startStreamingOnConnect) {
        if (board == null) {
            return;
        }
        final BoardConnectionHandler.BoardConnectionListener listener = new BoardConnectionHandler.BoardConnectionListener() {
            @Override
            public void onBoardConnected(final MetaWearBoard board) {
                readBatteryState(board);
                notifyBoardConnectionStatus(board, true);
                connectedBoardsInfo.put(board.getMacAddress(),
                        new BoardStateInfo(board, ConnectionStatus.CONNECTED));
                if (startStreamingOnConnect) {
                    doBoardSensorsConnect(board);
                }
            }

            @Override
            public void onBoardDisconnected(final MetaWearBoard board) {
                // Refresh a board's state info
                final BoardStateInfo boardStateInfo = connectedBoardsInfo.get(board.getMacAddress());
                if (boardStateInfo != null) {
                    boardStateInfo.connectionStatus = ConnectionStatus.DISCONNECTED;
                } else {
                    connectedBoardsInfo.put(board.getMacAddress(),
                            new BoardStateInfo(board, ConnectionStatus.DISCONNECTED));
                }

                notifyBoardConnectionStatus(board, false);

                // Check if this board is still selected
                boolean selected = isBoardSelected(board);
                if (selected) {
                    reconnect(board);
                } else {
                    doBoardDisconnect(board);
                }
            }

            @Override
            public void onConnectionFailed(final MetaWearBoard board) {
                // Refresh a board's state info
                final BoardStateInfo boardStateInfo = connectedBoardsInfo.get(board.getMacAddress());
                if (boardStateInfo != null) {
                    boardStateInfo.connectionStatus = ConnectionStatus.DISCONNECTED;
                } else {
                    connectedBoardsInfo.put(board.getMacAddress(),
                            new BoardStateInfo(board, ConnectionStatus.DISCONNECTED));
                }

                notifyBoardConnectionStatus(board, false);
                boolean selected = isBoardSelected(board);
                if (selected) {
                    reconnect(board);
                } else {
                    doBoardDisconnect(board);
                }
            }

            @Override
            public void onNewDataRecord(final MetaWearBoard board, final BoardReading reading) {
                if (boardRecordsMap == null) {
                    return;
                }
                if (captureSession == null) {
                    return;
                }
                reading.setSessionId(captureSession.getId());
                // Put a new record into a processing queue for the consumer thread to process it
                final Queue<BoardReading> records = boardRecordsMap.get(board.getMacAddress());
                if (records != null) {
                    records.add(reading);
                }
            }
        };

        final BoardConnectionHandler connectionHandler = new BoardConnectionHandler(board);
        connectionHandler.setListener(listener);
        boardConnectionHandlers.put(board.getMacAddress(), connectionHandler);
        connectionHandler.connect();
    }

    private void processNewReading(final BoardRecordBundle bundle) {
//        if (BuildConfig.DEBUG) {
//            if (Looper.myLooper() != taskPosterHandler.getLooper()) {
//                throw new IllegalStateException(LOG_TAG + ": maybe you've got lost in threads." +
//                        " Don't use whatever thread you're on to post events." );
//            }
//        }
        if (bundle == null) {
            return;
        }
         // Attempt to prevent the UI thread from hanging after 10-20 minutes.
        taskPosterHandler.post(new Runnable() {
            @Override
            public void run() {
                if (csvExporter != null) {
                    csvExporter.processReadingBundle(bundle);
                }
            }
        });
    }

    private boolean isBoardSelected(final MetaWearBoard board) {
        return selectedBoards.containsKey(board.getMacAddress());
    }

    private void disconnectBoard(final MetaWearBoard board) {
        if (board == null) {
            return;
        }
        board.tearDown();
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.remove(board.getMacAddress());
        if (connectionHandler != null) {
            connectionHandler.stopAccelerometerStreaming();
            connectionHandler.stopGyroscopeStreaming();
            connectionHandler.stopMagnetometerStreaming();
            connectionHandler.stop();
        }
    }

    protected void doBoardSensorsConnect(final MetaWearBoard board) {
        if (board == null) {
            return;
        }

        final Accelerometer accelerometer = retrieveAccelerometer(board);
        if (accelerometer != null) {
            final boolean status = startAccelerometer(board, accelerometer);
            if (status) {
                startAccelerometerStreaming(board, accelerometer);
            }
        } else {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + NO_ACCELEROMETER_MESSAGE);
        }

        final Gyro gyroModule = retrieveGyroscope(board);
        if (gyroModule != null) {
            final boolean status = startGyroscope(board, gyroModule);
            if (status) {
                startGyroscopeStreaming(board, gyroModule);
            }
        } else {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + NO_GYROSCOPE_MESSAGE);
        }

        final Bmm150Magnetometer magnetometer = retrieveMagnetometer(board);
        if (magnetometer != null) {
            final boolean status = startMagnetometer(board, magnetometer);
            if (status) {
                startMagnetometerStreaming(board, magnetometer);
            }
        } else {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + NO_MAGNETOMETER_MESSAGE);
        }
    }

    protected void doBoardDisconnect(MetaWearBoard board) {
        if (board == null) {
            return;
        }

        disconnectBoard(board);
    }

    private void readBatteryState(final MetaWearBoard board) {
        board.readBatteryLevel().onComplete(new AsyncOperation.CompletionHandler<Byte>() {
            @Override
            public void success(final Byte result) {
                EventBus.getDefault().post(new BatteryUpdateEvent(board.getMacAddress(), result));
                Trace.d(LOG_TAG + ": " + board.getMacAddress() + " battery state received." +
                        " Charge=" + result);
            }

            @Override
            public void failure(final Throwable error) {
                Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to read a battery state."
                        + ExceptionUtils.getMessage(error));
            }
        });
    }

    private void notifyBoardConnectionStatus(final MetaWearBoard board, boolean isConnected) {
        if (board == null) {
            return;
        }

        final String address = board.getMacAddress();

        // TODO There are two types of events which is confusing. Leave only one of them in the program.

        final ConnectionStateChangedReport event = new ConnectionStateChangedReport(address,
                isConnected ? ConnectionStatus.CONNECTED : ConnectionStatus.DISCONNECTED);
        EventBus.getDefault().post(event);
        if (isConnected) {
            EventBus.getDefault().post(new BoardConnectedEvent(address));
        } else {
            EventBus.getDefault().post(new BoardDisconnectedEvent(address));
        }
    }

    private boolean startAccelerometer(final MetaWearBoard board, final Accelerometer accelerometer) {
        if (accelerometer == null) {
            throw new IllegalStateException(LOG_TAG + ": accelerometer module must not be null");
        }
        accelerometer.enableAxisSampling();
        accelerometer.setAxisSamplingRange(PrefHelper.getAccelerometerAccelerationRange(this).scale());
        accelerometer.setOutputDataRate(PrefHelper.getAccelerometerOutputDataRate(this).frequency());
        try {
            accelerometer.start();
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " "
                    + ACCELEROMETER_CLASS.getSimpleName() + " started");
        } catch (Exception e) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " failed to start "
                    + ACCELEROMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return false;
        }
        return true;
    }

    private void startAccelerometerStreaming(final MetaWearBoard board, Accelerometer accelerometer) {
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.get(board.getMacAddress());
        connectionHandler.startAccelerometerStreaming(accelerometer);
    }

    protected void stopAccelerometer(final MetaWearBoard board) {
        boolean success = false;
        final Accelerometer accelerometer = retrieveAccelerometer(board);
        if (accelerometer == null) {
            return;
        }

        try {
            accelerometer.disableAxisSampling();
        } catch (Exception e) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " stopAccelerometer: "
                    + ACCELEROMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }

        try {
            accelerometer.stop();
            success = true;
        } catch (Exception e) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " stopAccelerometer: "
                    + ACCELEROMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }

        if (success) {
            Trace.d((LOG_TAG + ": " + board.getMacAddress() + " "
                    + ACCELEROMETER_CLASS.getSimpleName() + " stopped"));
        }
    }

    private void startGyroscopeStreaming(final MetaWearBoard board, final Gyro gyroModule) {
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.get(board.getMacAddress());
        connectionHandler.startGyroscopeStreaming(gyroModule);
    }

    private boolean startGyroscope(final MetaWearBoard board, final Gyro gyroModule) {
        if (gyroModule == null) {
            throw new IllegalStateException(LOG_TAG + ": gyroscope module must not be null");
        }

        try {
            gyroModule.setAngularRateRange(PrefHelper.getGyroscopeFullScaleRange(this).scale());
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " " + GYROSCOPE_CLASS.getSimpleName()
                    + " " + ExceptionUtils.getMessage(e));
        }
        try {
            gyroModule.setOutputDataRate(PrefHelper.getGyroscopeOutputDataRate(this));
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " " + GYROSCOPE_CLASS.getSimpleName()
                    + " " + ExceptionUtils.getMessage(e));
        }
        try {
            gyroModule.start();
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to start "
                    + GYROSCOPE_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return false;
        }
        Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + GYROSCOPE_CLASS.getSimpleName()
                + " started");
        return true;
    }

    protected void stopGyroscope(final MetaWearBoard board) {
        boolean success = false;
        Gyro gyroModule = retrieveGyroscope(board);

        if (gyroModule == null) {
            return;
        }

        try {
            gyroModule.stop();
            success = true;
        } catch (Exception e) {
            Trace.d(LOG_TAG + board.getMacAddress() + " stopGyroscope: "
                    + GYROSCOPE_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }

        if (success) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + GYROSCOPE_CLASS.getSimpleName()
                    + " stopped");
        }
    }

    private boolean startMagnetometer(final MetaWearBoard board, final Bmm150Magnetometer magnetometer) {
        if (magnetometer == null) {
            throw new IllegalStateException(LOG_TAG + ": magnetometer module must not be null");
        }
        try {
            // Set to low power mode
            magnetometer.setPowerPreset(Bmm150Magnetometer.PowerPreset.LOW_POWER);
        } catch (Exception e) {
            Trace.e(LOG_TAG + board.getMacAddress() + " startMagnetometer: "
                    + MAGNETOMETER_CLASS.getSimpleName() + " " + ExceptionUtils.getMessage(e));
        }
        try {
            magnetometer.enableBFieldSampling();
        } catch (Exception e) {
            Trace.e(LOG_TAG + board.getMacAddress() + " startMagnetometer: "
                    + MAGNETOMETER_CLASS.getSimpleName() + " " + ExceptionUtils.getMessage(e));
        }
        try {
            magnetometer.start();
        } catch (Exception e) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " failed to start "
                    + MAGNETOMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return false;
        }
        Trace.d(LOG_TAG + ": " + board.getMacAddress() + " " + MAGNETOMETER_CLASS.getSimpleName()
                + " started" );
        return true;
    }

    private void startMagnetometerStreaming(final MetaWearBoard board, final Bmm150Magnetometer magnetometer) {
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.get(board.getMacAddress());
        connectionHandler.startMagnetometerStreaming(magnetometer);
    }

    protected void stopMagnetometer(final MetaWearBoard board) {
        boolean success = false;
        Bmm150Magnetometer magnetometer = retrieveMagnetometer(board);

        if (magnetometer == null) {
            return;
        }

        try {
            magnetometer.disableBFieldSampling();
        } catch (Exception e) {
            Trace.d(LOG_TAG + board.getMacAddress() + " stopMagnetometer: "
                    + MAGNETOMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }
        try {
            magnetometer.stop();
            success = true;
        } catch (Exception e) {
            Trace.d(LOG_TAG + board.getMacAddress() + " stopMagnetometer: "
                    + MAGNETOMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
        }

        if (success) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " "
                    + MAGNETOMETER_CLASS.getSimpleName() + " stopped");
        }
    }


    @Nullable
    private Accelerometer retrieveAccelerometer(final MetaWearBoard board) {
        Accelerometer accelerometer;
        try {
            accelerometer = board.getModule(ACCELEROMETER_CLASS);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": "+ board.getMacAddress() + " failed to get "
                    + ACCELEROMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return null;
        }

        return accelerometer;
    }

    @Nullable
    private Gyro retrieveGyroscope(final MetaWearBoard board) {
        Gyro gyroModule;
        try {
            gyroModule = board.getModule(GYROSCOPE_CLASS);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to get "
                    + GYROSCOPE_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return null;
        }

        return gyroModule;
    }

    @Nullable
    private Bmm150Magnetometer retrieveMagnetometer(final MetaWearBoard board) {
        Bmm150Magnetometer magnetometer;
        try {
            magnetometer = board.getModule(MAGNETOMETER_CLASS);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to get "
                    + MAGNETOMETER_CLASS.getSimpleName() + ". " + ExceptionUtils.getMessage(e));
            return null;
        }
        return magnetometer;
    }

    private int getFreeSlot(final String address) {
        if (deviceToSlotMapping.isEmpty()) {
            return 1;
        }

        if (deviceToSlotMapping.containsKey(address)) {
            return deviceToSlotMapping.get(address);
        }

        final Collection<Integer> allSlots = deviceToSlotMapping.values();
        final Integer maxNumber = Collections.max(allSlots);

        for (int i = 1; i < maxNumber + 1; i++) {
            if (!allSlots.contains(i)) {
                return i;
            }
        }

        return maxNumber + 1;
    }

    private void reconnect(final MetaWearBoard board) {
        final BoardConnectionHandler connectionHandler = boardConnectionHandlers.get(board.getMacAddress());
        if (connectionHandler == null) {
            return;
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Trace.d(LOG_TAG + ": " + board.getMacAddress() + " reconnect attempt ");
                connectionHandler.connect();
                doBoardSensorsConnect(board);
            }
        }, BOARD_RECONNECT_TIMEOUT_MILLIS);

        Trace.d(LOG_TAG + ": board " + board.getMacAddress() + " reconnect is scheduled in "
                + String.valueOf((float) BOARD_RECONNECT_TIMEOUT_MILLIS / 1000.0f) + " seconds");
    }

    public boolean isDataRecordingInProgress() {
        return isDataRecordingInProgress.get();
    }

    private static int[] calculateTimeSpans(int timeFractionMillis, final float dataFrequency) {
        final int count = Float.valueOf(dataFrequency).intValue();
        int currentTick = timeFractionMillis;
        final int[] timeFractions = new int[count];

        for (int i = 0; i < count || currentTick < 1000; i++) {
            currentTick += timeFractionMillis;
            timeFractions[i] = currentTick;
        }
        return timeFractions;
    }

    public static class BoardStateInfo {
        final MetaWearBoard board;
        ConnectionStatus connectionStatus = ConnectionStatus.UNKNOWN;

        public BoardStateInfo(final MetaWearBoard board) {
            this(board, ConnectionStatus.UNKNOWN);
        }

        public BoardStateInfo(final MetaWearBoard board, final ConnectionStatus status) {
            this.board = board;
            connectionStatus = status;
        }

        public MetaWearBoard getBoard() {
            return board;
        }

        public ConnectionStatus getConnectionStatus() {
            return connectionStatus;
        }
    }

    private class MetaWearServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(final ComponentName componentName, final IBinder iBinder) {
            metaWearServiceBinder = (MetaWearBleService.LocalBinder) iBinder;
            metaWearServiceBinder.executeOnUiThread();
            Trace.d(LOG_TAG + ": " + MetaWearBleService.class.getSimpleName() + " connected");
        }

        @Override
        public void onServiceDisconnected(final ComponentName componentName) {
            metaWearServiceBinder = null;
            Trace.d(LOG_TAG + ": " + MetaWearBleService.class.getSimpleName() + " disconnected");
        }
    }

    public class MetaWearReaderServiceBinder extends Binder {
        private final MetaWearStreamingService service = MetaWearStreamingService.this;

        public final MetaWearStreamingService getService() {
            return service;
        }
    }

    public static class BoardRecordBundle {
        private final long sessionId;
        private Date timestamp;
        private final Map<String, BoardReading> boardRecords = new HashMap<>();

        public BoardRecordBundle(final long sessionId) {
            this.sessionId = sessionId;
        }

        public void addRecord(BoardReading record) {
            if (record == null) {
                return;
            }
            boardRecords.put(record.getBoardAddress(), record);
        }

        public Map<String, BoardReading> getBoardRecords() {
            return boardRecords;
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(final Date timestamp) {
            this.timestamp = timestamp;
        }

        public long getSessionId() {
            return sessionId;
        }
    }

}

