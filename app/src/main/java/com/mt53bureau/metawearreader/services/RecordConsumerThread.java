package com.mt53bureau.metawearreader.services;

import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.db.BoardReading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

class RecordConsumerThread extends Thread {

    private static final String LOG_TAG = BuildConfig.LOG_TAG + RecordConsumerThread.class.getSimpleName();
    private Date startTimestamp;
    private final long sessionId;
    private final Object obj = new Object();
    private Thread recordWaitingThread;
    private volatile boolean shouldStop;
    private final ConcurrentMap<String, BlockingDeque<BoardReading>> boardRecordsMapRef;
    private final int timeSpanMillis;
    private final ProcessCallback callback;
    private Thread self;

    public RecordConsumerThread(long sessionId, final ConcurrentMap<String, BlockingDeque<BoardReading>> boardRecordsMapRef, final int timeSpanMillis, ProcessCallback callback) {
        this.sessionId = sessionId;
        this.boardRecordsMapRef = boardRecordsMapRef;
        this.timeSpanMillis = timeSpanMillis;
        this.callback = callback;
    }

    @Override
    public void run() {
        self = Thread.currentThread();
        self.setName("record-consumer-thread-sID#" + sessionId);

        recordWaitingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName("record-waiting-thread-sID#" + sessionId);
                while (!Thread.currentThread().isInterrupted()) {
                    // Wait for first record to appear
                    boolean b = false;
                    for (Map.Entry<String, BlockingDeque<BoardReading>> entry : boardRecordsMapRef.entrySet()) {
                        final BlockingDeque<BoardReading> records = entry.getValue();
                        b = waitRecord(records, timeSpanMillis);
                        if (b) {
                            break;
                        }
                    }
                    if (b) {
                        break;
                    }
                }

                if (Thread.currentThread().isInterrupted()) {
                    return;
                }

                Date mostEarly = null;

                // Founded the first record. Now find minimum date between all of them
                for (Map.Entry<String, BlockingDeque<BoardReading>> entry : boardRecordsMapRef.entrySet()) {
                    final BlockingDeque<BoardReading> records = entry.getValue();
                    final BoardReading record = records.peekFirst();
                    if (record != null) {
                        final Date recordTimestamp = record.getTimestamp();
                        if (mostEarly == null) {
                            mostEarly = recordTimestamp;
                        } else {
                            if (recordTimestamp.before(mostEarly)) {
                                mostEarly = recordTimestamp;
                            }
                        }
                    }
                }

                synchronized (obj) {
                    startTimestamp = mostEarly;
                    obj.notifyAll();
                }
            }
        });
        recordWaitingThread.start();

        // Wait for first record to appear
        synchronized (obj) {
            while (startTimestamp == null) {
                try {
                    obj.wait(3000);
                } catch (InterruptedException e) {
                    if (shouldStop) {
                        Thread.currentThread().interrupt();
                    }
                }

                // Check if this thread is stopped already. A user may have pressed STOP
                // before any record was received from a board
                if (Thread.currentThread().isInterrupted()) {
                    if (recordWaitingThread != null) {
                        recordWaitingThread.interrupt();
                    }
                    return;
                }
            }
        }

        recordWaitingThread = null;
        long spanStart = startTimestamp.getTime();

        while (!Thread.currentThread().isInterrupted()) {
            final long spanEnd = spanStart + timeSpanMillis;
            final MetaWearStreamingService.BoardRecordBundle bundle = new MetaWearStreamingService.BoardRecordBundle(sessionId);

            for (Map.Entry<String, BlockingDeque<BoardReading>> entry : boardRecordsMapRef.entrySet()) {
                final String address = entry.getKey();
                final BlockingDeque<BoardReading> records = entry.getValue();

                if (Thread.currentThread().isInterrupted() || shouldStop) {
                    return;
                }

                final List<BoardReading> readings = takeBetween(records, spanStart, spanEnd, timeSpanMillis / 2);

                // No records for this board in a current time span
                if (readings.isEmpty()) {
                    continue;
                }

                final RecordWrapper reusableDataWrapper = new RecordWrapper();

                // Calculate average values
                for (final BoardReading r : readings) {
                    if (bundle.getTimestamp() == null) {
                        bundle.setTimestamp(r.getTimestamp());
                    }
                    final Float accX = r.getAccX();
                    if (accX != null) {
                        reusableDataWrapper.accX.add(accX);
                    }

                    final Float accY = r.getAccY();
                    if (accY != null) {
                        reusableDataWrapper.accY.add(accY);
                    }

                    final Float accZ = r.getAccZ();
                    if (accZ != null) {
                        reusableDataWrapper.accZ.add(accZ);
                    }

                    final Float gyrX = r.getGyrX();
                    if (gyrX != null) {
                        reusableDataWrapper.gyrX.add(gyrX);
                    }

                    final Float gyrY = r.getGyrY();
                    if (gyrY != null) {
                        reusableDataWrapper.gyrY.add(gyrY);
                    }

                    final Float gyrZ = r.getGyrZ();
                    if (gyrZ != null) {
                        reusableDataWrapper.gyrZ.add(gyrZ);
                    }

                    final Float magX = r.getMagX();
                    if (magX != null) {
                        reusableDataWrapper.magX.add(magX);
                    }

                    final Float magY = r.getMagY();
                    if (magY != null) {
                        reusableDataWrapper.magY.add(magY);
                    }

                    final Float magZ = r.getMagZ();
                    if (magZ != null) {
                        reusableDataWrapper.magZ.add(magZ);
                    }
                }

                final BoardReading reading = new BoardReading(null,
                        address,
                        bundle.getTimestamp(),
                        reusableDataWrapper.getAvgAccX(),
                        reusableDataWrapper.getAvgAccY(),
                        reusableDataWrapper.getAvgAccZ(),
                        reusableDataWrapper.getAvgGyrX(),
                        reusableDataWrapper.getAvgGyrY(),
                        reusableDataWrapper.getAvgGyrZ(),
                        reusableDataWrapper.getAvgMagX(),
                        reusableDataWrapper.getAvgMagY(),
                        reusableDataWrapper.getAvgMagZ(),
                        sessionId);
                bundle.addRecord(reading);

                if (Thread.currentThread().isInterrupted() || shouldStop) {
                    break;
                }
            }

            if (bundle.getBoardRecords().size() <= 0) {
                bundle.setTimestamp(new Date(spanStart));
            }

            if (callback != null) {
                callback.onRecordMeasured(bundle);
            }

            spanStart = spanEnd + 1;
        }
    }

    public void shouldStop() {
        shouldStop = true;
        self.interrupt();
    }

    public interface ProcessCallback {
        void onRecordMeasured(MetaWearStreamingService.BoardRecordBundle bundle);
    }

    private static boolean waitRecord(final BlockingDeque<BoardReading> queue, final long timeoutMillis) {
        final BoardReading r;
        try {
            r = queue.getFirst();
        } catch (NoSuchElementException e) {
            return false;
        }
        return r != null;
    }

    private static List<BoardReading> takeBetween(final BlockingDeque<BoardReading> queue, final long startTime, final long endTime, final long waitTimeoutMillis) {
        if (queue == null) {
            return Collections.emptyList();
        }
        final List<BoardReading> result = new ArrayList<>();
        while (true) {
            BoardReading r = null;
            try {
                r = queue.pollFirst(waitTimeoutMillis, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                /* Noticed */
            }

            // Queue is empty
            if (r == null) {
                break;
            }

            final long recordTime = r.getTimestamp().getTime();
            if (recordTime >= startTime && recordTime < endTime) {
                result.add(r);
            } else if (recordTime > endTime) {
                break;
            }
        }
        return result;
    }

}
