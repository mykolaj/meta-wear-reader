package com.mt53bureau.metawearreader.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelUuid;

import com.mbientlab.metawear.MetaWearBoard;
import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.events.DeviceDiscoveredEvent;
import com.mt53bureau.metawearreader.events.DeviceScanFinishedEvent;
import com.mt53bureau.metawearreader.events.DeviceScanStartedEvent;
import com.mt53bureau.metawearreader.model.ScannedDeviceInfo;

import org.greenrobot.eventbus.EventBus;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


public class BleScannerService extends Service {

    private static final String LOG_TAG = String.format("%s.%s", BuildConfig.LOG_TAG, BleScannerService.class.getSimpleName());
    private static final int BLE_SCAN_TIMEOUT = 1000 * 15;
    private static final UUID[] META_WEAR_BOARD_SERVICE_UUIDS = new UUID[]{
            MetaWearBoard.METAWEAR_SERVICE_UUID,
            MetaWearBoard.METABOOT_SERVICE_UUID
    };
    private final BleScannerServiceBinder serviceBinder = new BleScannerServiceBinder();
    private final Set<ScannedDeviceInfo> discoveredDevices = new HashSet<>();

    /** Indicates if a scanning is in progress or not */
    private boolean isScanning;
    private Set<UUID> filterServiceUuids;
    private Set<ParcelUuid> api21FilterServiceUuids;
    private BluetoothAdapter.LeScanCallback deprecatedScanCallback;
    private ScanCallback api21ScanCallback;
    private BluetoothAdapter btAdapter;
    private Handler taskHandler;

    public static void start(Context context) {
        Intent starter = new Intent(context, BleScannerService.class);
        context.startService(starter);
    }

    public static void stop(Context context) {
        Intent starter = new Intent(context, BleScannerService.class);
        context.stopService(starter);
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return serviceBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        btAdapter = ((BluetoothManager) getSystemService(BLUETOOTH_SERVICE)).getAdapter();
        taskHandler = new Handler();
        final UUID[] filterUuids = META_WEAR_BOARD_SERVICE_UUIDS;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            filterServiceUuids = new HashSet<>();
            //noinspection ConstantConditions
            if (filterUuids != null) {
                filterServiceUuids.addAll(Arrays.asList(filterUuids));
            }
        } else {
            api21FilterServiceUuids = new HashSet<>();
            for (UUID uuid : filterUuids) {
                api21FilterServiceUuids.add(new ParcelUuid(uuid));
            }
        }
    }

    /**
     * Starts scanning for Bluetooth LE devices
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public void startBleScan() {
        if (btAdapter == null) {
            return;
        }
        final boolean enabled = btAdapter.isEnabled();
        // UI must handle a Bluetooth adapter first
        if (!enabled) {
            return;
        }

        isScanning = true;
        EventBus.getDefault().post(new DeviceScanStartedEvent());
        taskHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopBleScan();
            }
        }, BLE_SCAN_TIMEOUT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            deprecatedScanCallback = new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] scanRecord) {
                    ByteBuffer buffer = ByteBuffer.wrap(scanRecord).order(ByteOrder.LITTLE_ENDIAN);
                    boolean stop = false;
                    while (!stop && buffer.remaining() > 2) {
                        byte length = buffer.get();
                        if (length == 0) break;

                        byte type = buffer.get();
                        switch (type) {
                            case 0x02: // Partial list of 16-bit UUIDs
                            case 0x03: // Complete list of 16-bit UUIDs
                                while (length >= 2) {
                                    UUID serviceUUID = UUID.fromString(String.format("%08x-0000-1000-8000-00805f9b34fb", buffer.getShort()));
                                    stop = filterServiceUuids.isEmpty() || filterServiceUuids.contains(serviceUUID);
                                    if (stop) {
                                        onDeviceDiscovered(bluetoothDevice, rssi);
                                    }

                                    length -= 2;
                                }
                                break;

                            case 0x06: // Partial list of 128-bit UUIDs
                            case 0x07: // Complete list of 128-bit UUIDs
                                while (!stop && length >= 16) {
                                    long lsb = buffer.getLong(), msb = buffer.getLong();
                                    stop = filterServiceUuids.isEmpty() || filterServiceUuids.contains(new UUID(msb, lsb));
                                    if (stop) {
                                        onDeviceDiscovered(bluetoothDevice, rssi);
                                    }
                                    length -= 16;
                                }
                                break;

                            default:
                                buffer.position(buffer.position() + length - 1);
                                break;
                        }
                    }

                    if (!stop && filterServiceUuids.isEmpty()) {
                        onDeviceDiscovered(bluetoothDevice, rssi);
                    }
                }
            };
            btAdapter.startLeScan(deprecatedScanCallback);
        } else {
            api21ScanCallback = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, final ScanResult result) {

                    Trace.d(LOG_TAG + ": found device: address=" + result.getDevice().getAddress()
                            + ", name=" + result.getDevice().getName());

                    if (result.getScanRecord() != null && result.getScanRecord().getServiceUuids() != null) {
                        boolean valid = true;
                        for (ParcelUuid it : result.getScanRecord().getServiceUuids()) {
                            valid &= api21FilterServiceUuids.contains(it);
                        }

                        if (valid) {
                            onDeviceDiscovered(result.getDevice(), result.getRssi());
                        }
                    }

                    super.onScanResult(callbackType, result);
                }
            };
            btAdapter.getBluetoothLeScanner().startScan(api21ScanCallback);
        }
    }

    private void onDeviceDiscovered(final BluetoothDevice device, final int rssi) {
        final ScannedDeviceInfo info = new ScannedDeviceInfo(device.getAddress(), device.getName(), rssi);
        discoveredDevices.add(info);
        EventBus.getDefault().post(new DeviceDiscoveredEvent(info));
    }

    private void onScanFinished() {
        EventBus.getDefault().post(new DeviceScanFinishedEvent());
    }

    /**
     * Stops the Bluetooth LE scan
     */
    public void stopBleScan() {
        if (isScanning && btAdapter.isEnabled()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                btAdapter.stopLeScan(deprecatedScanCallback);
            } else {
                btAdapter.getBluetoothLeScanner().stopScan(api21ScanCallback);
            }
        }
        isScanning = false;
        onScanFinished();
    }

    public void onScanButtonClicked() {
        if (isScanning) {
            stopBleScan();
        } else {
            startBleScan();
        }
    }

    public boolean isScanning() {
        return isScanning;
    }

    public Collection<ScannedDeviceInfo> getDiscoveredDevices() {
        return discoveredDevices;
    }

    public class BleScannerServiceBinder extends Binder {
        public BleScannerService getService() {
            return BleScannerService.this;
        }
    }

}
