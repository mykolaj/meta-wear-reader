package com.mt53bureau.metawearreader.services;

import java.util.Locale;

import gnu.trove.list.array.TFloatArrayList;

import static com.mt53bureau.metawearreader.CsvFormatters.FLOAT_FORMATTER;

class RecordWrapper {
    private static final int DEFAULT_CAPACITY = 200; // This value is the result of testing on 50 Hz

    public final TFloatArrayList accX = new TFloatArrayList(DEFAULT_CAPACITY);
    public final TFloatArrayList accY = new TFloatArrayList(DEFAULT_CAPACITY);
    public final TFloatArrayList accZ = new TFloatArrayList(DEFAULT_CAPACITY);
    public final TFloatArrayList gyrX = new TFloatArrayList(DEFAULT_CAPACITY);
    public final TFloatArrayList gyrY = new TFloatArrayList(DEFAULT_CAPACITY);
    public final TFloatArrayList gyrZ = new TFloatArrayList(DEFAULT_CAPACITY);
    public final TFloatArrayList magX = new TFloatArrayList(DEFAULT_CAPACITY);
    public final TFloatArrayList magY = new TFloatArrayList(DEFAULT_CAPACITY);
    public final TFloatArrayList magZ = new TFloatArrayList(DEFAULT_CAPACITY);

    public void reset() {
        accX.resetQuick();
        accY.resetQuick();
        accZ.resetQuick();
        gyrX.resetQuick();
        gyrY.resetQuick();
        gyrZ.resetQuick();
        magX.resetQuick();
        magY.resetQuick();
        magZ.resetQuick();
    }

    public Float getAvgAccX() {
        if (accX.size() > 0) {
            return accX.sum() / accX.size();
        } else {
            return null;
        }
    }

    public Float getAvgAccY() {
        if (accY.size() > 0) {
            return accY.sum() / accY.size();
        } else {
            return null;
        }
    }

    public Float getAvgAccZ() {
        if (accZ.size() > 0) {
            return accZ.sum() / accZ.size();
        } else {
            return null;
        }
    }

    public Float getAvgGyrX() {
        if (gyrX.size() > 0) {
            return gyrX.sum() / gyrX.size();
        } else {
            return null;
        }
    }

    public Float getAvgGyrY() {
        if (gyrY.size() > 0) {
            return gyrY.sum() / gyrY.size();
        } else {
            return null;
        }
    }

    public Float getAvgGyrZ() {
        if (gyrZ.size() > 0) {
            return gyrZ.sum() / gyrZ.size();
        } else {
            return null;
        }
    }

    public Float getAvgMagX() {
        if (magX.size() > 0) {
            return magX.sum() / magX.size();
        } else {
            return null;
        }
    }

    public Float getAvgMagY() {
        if (magY.size() > 0) {
            return magY.sum() / magY.size();
        } else {
            return null;
        }
    }

    public Float getAvgMagZ() {
        if (magZ.size() > 0) {
            return magZ.sum() / magZ.size();
        } else {
            return null;
        }
    }

    public static String toCsv(RecordWrapper r) {
        if (r == null) {
            return ",,,,,,,,"; // "acc-x,acc-y,acc-z,gyr-x,gyr-y,gyr-z,mag-x,mag-y,mag-z"
        }
        return String.format(Locale.US, "%s,%s,%s,%s,%s,%s,%s,%s,%s",
                r.accX.size() > 0 ? FLOAT_FORMATTER.format(r.accX.sum() / r.accX.size()) : "",
                r.accY.size() > 0 ? FLOAT_FORMATTER.format(r.accY.sum() / r.accY.size()) : "",
                r.accZ.size() > 0 ? FLOAT_FORMATTER.format(r.accZ.sum() / r.accZ.size()) : "",
                r.gyrX.size() > 0 ? FLOAT_FORMATTER.format(r.gyrX.sum() / r.gyrX.size()) : "",
                r.gyrY.size() > 0 ? FLOAT_FORMATTER.format(r.gyrY.sum() / r.gyrY.size()) : "",
                r.gyrZ.size() > 0 ? FLOAT_FORMATTER.format(r.gyrZ.sum() / r.gyrZ.size()) : "",
                r.magX.size() > 0 ? FLOAT_FORMATTER.format(r.magX.sum() / r.magX.size()) : "",
                r.magY.size() > 0 ? FLOAT_FORMATTER.format(r.magY.sum() / r.magY.size()) : "",
                r.magZ.size() > 0 ? FLOAT_FORMATTER.format(r.magZ.sum() / r.magZ.size()) : ""
        );
    }
}
