package com.mt53bureau.metawearreader;

import android.support.multidex.MultiDexApplication;

import com.mt53bureau.metawearreader.db.DaoMaster;
import com.mt53bureau.metawearreader.db.DaoSession;
import com.mt53bureau.metawearreader.db.DiskDbOpenHelper;
import com.mt53bureau.metawearreader.db.MemoryDbOpenHelper;

import net.danlew.android.joda.JodaTimeAndroid;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.greendao.query.QueryBuilder;

@SuppressWarnings("FieldCanBeLocal")
@ReportsCrashes(formUri = "https://collector.tracepot.com/14aa3ecb")
public class CurrentApplication extends MultiDexApplication {
    private DaoSession diskDbDaoSession;
    private DaoMaster diskDbDaoMaster;
    private DaoMaster memoryDbDaoMaster;
    private DaoSession memoryDbDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG) {
            ACRA.init(this);
        }
        JodaTimeAndroid.init(this);
        final DaoMaster.OpenHelper diskDbHelper = new DiskDbOpenHelper(this, "readings-db", null);
        diskDbDaoMaster = new DaoMaster(diskDbHelper.getWritableDb());
        diskDbDaoSession = diskDbDaoMaster.newSession();

        // In-memory DB. Not in use so far. Maybe it would not be necessary to use it at all.
        final DaoMaster.OpenHelper memoryDbHelper = new MemoryDbOpenHelper(this, null);
        memoryDbDaoMaster = new DaoMaster(memoryDbHelper.getWritableDb());
        memoryDbDaoSession = memoryDbDaoMaster.newSession();

        if (BuildConfig.DEBUG) {
            QueryBuilder.LOG_SQL = true;
            QueryBuilder.LOG_VALUES = true;
        }

        EventBus.builder().throwSubscriberException(false).installDefaultEventBus();
    }

    public DaoSession getDiskDaoSession() {
        return diskDbDaoSession;
    }

    public DaoSession getMemoryDaoSession() {
        return memoryDbDaoSession;
    }

}
