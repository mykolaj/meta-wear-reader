package com.mt53bureau.metawearreader;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.mt53bureau.metawearreader.db.BoardReading;
import com.mt53bureau.metawearreader.fileio.FileIo;
import com.mt53bureau.metawearreader.services.MetaWearStreamingService;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.mt53bureau.metawearreader.CsvFormatters.DATE_FORMATTER;
import static com.mt53bureau.metawearreader.CsvFormatters.MILLIS_FORMATTER;
import static com.mt53bureau.metawearreader.CsvFormatters.TIME_FORMATTER;

public class CsvExporter {

    private static final String LOG_TAG = BuildConfig.LOG_TAG + CsvExporter.class.getSimpleName();
    private final Map<String, BoardReading> addrToMostRecentReadingMap = new ConcurrentHashMap<>();
    private final long sessionId;
    private final String fileName;
    private final Handler csvIoHandler;
    private final SortedMap<Integer, String> slotMap;
    private OutputStream outputStream;
    private final AtomicBoolean shouldStop = new AtomicBoolean(false);

    /**
     * @param sessionId ID of a current capture session
     * @param fileName  A name of a file to save data to
     */
    public CsvExporter(final long sessionId, @Nullable final String fileName, Handler csvIoHandler, Map<String, Integer> deviceAddressToSlotMapping) {
        this.sessionId = sessionId;
        this.fileName = fileName;
        this.csvIoHandler = csvIoHandler;
        // Write all board's readings in the same order they were selected by a user
        slotMap = new TreeMap<>(new Comparator<Integer>() {
            @Override
            public int compare(final Integer lhs, final Integer rhs) {
                return lhs.compareTo(rhs);
            }
        });
        // Swap places of keys and values
        for (Map.Entry<String, Integer> entry : deviceAddressToSlotMapping.entrySet()) {
            slotMap.put(entry.getValue(), entry.getKey());
        }
    }

    private File openFile(final Context context) {
        final File file;
        if (TextUtils.isEmpty(fileName)) {
            file = FileIo.prepareOutputFileForSession(context, sessionId);
        } else {
            file = FileIo.prepareOutputFile(context, sessionId, fileName);
        }
        outputStream = null;
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(file, true));
        } catch (IOException e) {
            Trace.e(LOG_TAG + ":  failed to open file. " + ExceptionUtils.getMessage(e));
            return null;
        }
        return file;
    }

    private void writeToFile(String s) {
        if (s == null) {
            return;
        }
        try {
            if (outputStream != null) {
                // Use UTF-8 since we need to write a sensor name to a file which can be named using any language in the world
                outputStream.write(s.getBytes("UTF-8"));
            }
        } catch (IOException e) {
            Trace.e(LOG_TAG + ": " + ExceptionUtils.getMessage(e));
        }
    }

    /**
     * Closes a file output stream if it's opened
     */
    private void closeFile() {
        if (outputStream == null) {
            return;
        }
        try {
            outputStream.close();
            outputStream = null;
        } catch (IOException e) {
            Trace.w(LOG_TAG + ": " + ExceptionUtils.getMessage(e));
        }
    }

    public void prepare(@NonNull final Context context, @NonNull final Map<String, BluetoothDevice> boards) {
        final File file = openFile(context);
        if (file != null) {
            boolean needWriteHeader = file.length() == 0;
            if (needWriteHeader) {
                StringBuilder sb = new StringBuilder(128);
                sb.append("Sensor,,,"); // Date, Time, Millis
                Iterator<Map.Entry<Integer, String>> slotIterator = slotMap.entrySet().iterator();
                while (slotIterator.hasNext()) {
                    final Map.Entry<Integer, String> entry = slotIterator.next();
                    final String boardAddress = entry.getValue();
                    final BluetoothDevice device = boards.get(boardAddress);
                    if (device != null) {
                        sb.append(String.format("%s %s,,,,,,,,", device.getName(), device.getAddress()));
                    }
                    if (slotIterator.hasNext()) {
                        sb.append(",\t");
                    } else {
                        sb.append("\n");
                    }
                }
                writeToFile(sb.toString());

                StringBuilder sb1 = new StringBuilder();
                sb1.append("Date,Time,Millis,");
                slotIterator = slotMap.entrySet().iterator();
                while (slotIterator.hasNext()) {
                    final Map.Entry<Integer, String> entry = slotIterator.next();
                    final int num = entry.getKey();
                    sb1.append(String.format(Locale.US, "Acc%1$dX,Acc%1$dY,Acc%1$dZ,Gyr%1$dX,Gyr%1$dY,Gyr%1$dZ,Mag%1$dX,Mag%1$dY,Mag%1$dZ", num));
                    if (slotIterator.hasNext()) {
                        sb1.append(",\t");
                    } else {
                        sb1.append("\n");
                    }
                }
                writeToFile(sb1.toString());
            }
        }
    }

    public void shouldClose() {
        shouldStop.set(true);
    }

    public void processReadingBundle(final MetaWearStreamingService.BoardRecordBundle bundle) {
        if (bundle == null) {
            return;
        }
        csvIoHandler.post(new Runnable() {
            @Override
            public void run() {
                if (shouldStop.get()) {
                    closeFile();
                    return;
                }

                // Ignore records from a wrong session if somehow they get here
                if (bundle.getSessionId() != sessionId) {
                    Trace.w(LOG_TAG + " processReadingBundle: record from wrong session was ignored");
                    return;
                }

                final Map<String, BoardReading> boardRecords = bundle.getBoardRecords();
                final long recordTime = bundle.getTimestamp().getTime();

                // Replace existing most recent board record with a new one to be able to process it when it's time
                for (BoardReading record : boardRecords.values()) {
                    final String addr = record.getBoardAddress();
                    addrToMostRecentReadingMap.remove(addr);
                    addrToMostRecentReadingMap.put(addr, record);
                }

                writeToFile(DATE_FORMATTER.print(recordTime));
                writeToFile(",");
                writeToFile(TIME_FORMATTER.print(recordTime));
                writeToFile(",");
                writeToFile(MILLIS_FORMATTER.format(recordTime));
                writeToFile(",\t");

                // Print board records in order they were selected by a user
                final Iterator<Map.Entry<Integer, String>> slotIterator = slotMap.entrySet().iterator();
                while (slotIterator.hasNext()) {
                    final Map.Entry<Integer, String> entry = slotIterator.next();
                    final String boardAddress = entry.getValue();
                    final BoardReading record = boardRecords.get(boardAddress);

                    if (record != null) {
                        writeToFile(BoardReading.toCsv(record));
                    } else {
                        final BoardReading r = addrToMostRecentReadingMap.get(boardAddress);
                        writeToFile(BoardReading.toCsv(r));
                    }

                    if (slotIterator.hasNext()) {
                        writeToFile(",\t");
                    }
                }

                writeToFile("\n");
            }
        });
    }

}
