package com.mt53bureau.metawearreader.fileio;

import android.content.Context;
import android.text.TextUtils;

import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.settings.PrefHelper;
import com.mt53bureau.metawearreader.ui.SessionFileNameDialog;
import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;

import java.io.File;
import java.util.Locale;

public class FileIo {

    private static final String LOG_TAG = BuildConfig.LOG_TAG + "." + FileIo.class.getSimpleName();

    @SuppressWarnings("SpellCheckingInspection")
    public static final String FILES_DIR = "ArifMultiMetawear";

    public static File prepareOutputFileForSession(final Context context, final long sessionId) {
        final Storage storage = SimpleStorage.getExternalStorage();
        final boolean exists = storage.isDirectoryExists(FileIo.FILES_DIR);
        if (!exists) {
            final boolean status = storage.createDirectory(FileIo.FILES_DIR);
            if (!status) {
                Trace.e(LOG_TAG + ": failed to create directory " + FileIo.FILES_DIR);
            } else {
                Trace.d(LOG_TAG + ": directory " + FileIo.FILES_DIR + " created successfully");
            }
        }
        final String file = getOutputFileNameWithSuffix(context, Long.toString(sessionId));
        if (!storage.isFileExist(FileIo.FILES_DIR, file)) {
            storage.createFile(FileIo.FILES_DIR, file, "");
        }
        return storage.getFile(FileIo.FILES_DIR, file);
    }

    public static String getOutputFileNameWithSuffix(final Context context, final String suffix) {
        return String.format(Locale.US, "%s-%s.csv",
                PrefHelper.getOutputFileNamePrefix(context),
                suffix.replace(SessionFileNameDialog.ILLEGAL_FILE_NAME_REGEX, "_").trim());
    }

    public static File prepareOutputFile(final Context context, final long sessionId, final String fileName) {
        final Storage storage = SimpleStorage.getExternalStorage();
        final boolean exists = storage.isDirectoryExists(FileIo.FILES_DIR);
        if (!exists) {
            final boolean status = storage.createDirectory(FileIo.FILES_DIR);
            if (!status) {
                Trace.e(LOG_TAG + ": failed to create directory " + FileIo.FILES_DIR);
            } else {
                Trace.d(LOG_TAG + ": directory " + FileIo.FILES_DIR + " created successfully");
            }
        }
        final String file;
        if (TextUtils.isEmpty(fileName)) {
            file = getOutputFileNameWithSuffix(context, Long.toString(sessionId));
        } else {
            // Default locale is ok unless only english is used for file names
            file = String.format("%s.csv", fileName.trim());
        }
        if (!storage.isFileExist(FileIo.FILES_DIR, file)) {
            storage.createFile(FileIo.FILES_DIR, file, "");
        }
        return storage.getFile(FileIo.FILES_DIR, file);
    }

}
