package com.mt53bureau.metawearreader.datahandle;

import android.os.Handler;
import android.os.HandlerThread;

import com.mbientlab.metawear.MetaWearBoard;
import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.model.ModuleReadingWrapper;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.Queue;


public class CsvWriteHandler {
    private static final String LOG_TAG = String.format("%s.%s", BuildConfig.LOG_TAG, CsvWriteHandler.class.getSimpleName());
    private final Handler writerHandler;
    private final Queue<ModuleReadingWrapper> writeQueue = new LinkedList<>();
    private HandlerThread writerThread;
    private boolean isIdle = true;
    private OutputStream outputStream;
    private final MetaWearBoard board;
    private final File file;
    private boolean shouldStop;

    public CsvWriteHandler(final MetaWearBoard board, final File file) {
        this.board = board;
        this.file = file;
        writerThread = new HandlerThread(this.board.getMacAddress() + "-csv-writer-thread");
        writerThread.start();
        writerHandler = new Handler(writerThread.getLooper());
    }

    public void processQueue() {
        if (!isIdle) {
            return;
        }
        isIdle = false;
        ModuleReadingWrapper command = writeQueue.poll();
        while (command != null) {
            final ModuleReadingWrapper finalCommand = command;
            writerHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        outputStream.write(finalCommand.toString().getBytes("UTF-8"));
                        outputStream.write('\n');
                    } catch (IOException e) {
                        Trace.e(LOG_TAG + " processQueue: " + board.getMacAddress()
                                + " " + ExceptionUtils.getMessage(e));
                    }
                }
            });
            command = writeQueue.poll();
        }
        isIdle = true;
        if (shouldStop) {
            closeOutputStream();
        }
    }

    public void stop() {
        shouldStop = true;
        writerThread.getLooper().quitSafely();
    }

    private void openOutputStream() {
        Trace.d(LOG_TAG + ": " + board.getMacAddress() + " opening output stream" );
        if (outputStream != null) {
            throw new IllegalStateException(LOG_TAG + ": output stream is already opened."
                    + " Close it first before opening a new one");
        }
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(file, true));
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " output stream opened" );
        } catch (IOException e) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " failed to open file for board. "
                    + ExceptionUtils.getMessage(e));
        }
    }

    private void closeOutputStream() {
        Trace.d(LOG_TAG + " closeOutputStream: " + board.getMacAddress() + " closing output stream");
        if (outputStream == null) {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " stream is already closed");
            return;
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            Trace.w(LOG_TAG + ": " + board.getMacAddress() + " " + ExceptionUtils.getMessage(e));
        }
        outputStream = null;
        Trace.d(LOG_TAG + ": " + board.getMacAddress() + " stream closed");
    }

    public void start() {

    }
}
