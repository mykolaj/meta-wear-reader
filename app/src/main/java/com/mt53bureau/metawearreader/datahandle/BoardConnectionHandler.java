package com.mt53bureau.metawearreader.datahandle;

import com.mbientlab.metawear.Message;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.RouteManager;
import com.mbientlab.metawear.data.CartesianFloat;
import com.mbientlab.metawear.module.Accelerometer;
import com.mbientlab.metawear.module.Bmm150Magnetometer;
import com.mbientlab.metawear.module.Gyro;
import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.db.BoardReading;
import com.mt53bureau.metawearreader.model.ModuleReading;
import com.mt53bureau.metawearreader.services.MetaWearStreamingService;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.Calendar;


@SuppressWarnings("FieldCanBeLocal")
public class BoardConnectionHandler {

    private static final String LOG_TAG = String.format("%s.%s", BuildConfig.LOG_TAG, BoardConnectionHandler.class.getSimpleName());

    private final MetaWearBoard board;
    private AccelerometerHandler accelerometerHandler;
    private GyroscopeHandler gyroscopeHandler;
    private MagnetometerHandler magnetometerHandler;
    private BoardConnectionListener listener;
    private MetaWearBoard.ConnectionStateHandler connectionStateHandler;
    private ModuleReading lastAccelerometerReading;
    private ModuleReading lastGyroscopeReading;
    private ModuleReading lastMagnetometerReading;
    private Accelerometer accelerometer;
    private Gyro gyroscope;
    private Bmm150Magnetometer magnetometer;

    private final RouteManager.MessageHandler accelerometerMessageHandler = new AccelerometerMessageHandler();
    private final RouteManager.MessageHandler gyroscopeMessageHandler = new GyroscopeMessageHandler();
    private final RouteManager.MessageHandler magnetometerMessageHandler = new MagnetometerMessageHandler();

    public BoardConnectionHandler(MetaWearBoard board) {
        if (board == null) {
            throw new IllegalStateException("Board can not be null");
        }
        this.board = board;
        connectionStateHandler = new ConnectionStateHandlerInternal();
        this.board.setConnectionStateHandler(connectionStateHandler);
    }

    public BoardConnectionListener getListener() {
        return listener;
    }

    public void setListener(final BoardConnectionListener listener) {
        this.listener = listener;
    }

    public MetaWearBoard getBoard() {
        return board;
    }

    public void connect() {
        try {
            board.connect();
        } catch (Exception e) {
            Trace.e(LOG_TAG + "connect: " + board.getMacAddress() + " " + ExceptionUtils.getMessage(e));
        }
    }

    public void disconnectBoard() {
        try {
            board.disconnect();
        } catch (Exception e) {
            Trace.e(LOG_TAG + "disconnectBoard: " + ExceptionUtils.getMessage(e));
        }
    }

    public void startAccelerometerStreaming(Accelerometer accelerometer) {
        if (accelerometer == null) {
            return;
        }
        this.accelerometer = accelerometer;
        accelerometerHandler = new AccelerometerHandler(board, accelerometerMessageHandler);
        try {
            this.accelerometer.routeData()
                    .fromAxes()
                    .stream(MetaWearStreamingService.ACCELEROMETER_STREAM_KEY)
                    .commit()
                    .onComplete(accelerometerHandler);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + ExceptionUtils.getMessage(e) + " "
                    + ExceptionUtils.getStackTrace(e));
        }
    }

    public void startGyroscopeStreaming(Gyro gyroscope) {
        if (gyroscope == null) {
            return;
        }
        this.gyroscope = gyroscope;
        gyroscopeHandler = new GyroscopeHandler(board, gyroscopeMessageHandler);
        try {
            this.gyroscope.routeData()
                    .fromAxes()
                    .stream(MetaWearStreamingService.GYROSCOPE_STREAM_KEY)
                    .commit()
                    .onComplete(gyroscopeHandler);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + ExceptionUtils.getMessage(e) + " "
                    + ExceptionUtils.getStackTrace(e));
        }
    }

    public void startMagnetometerStreaming(final Bmm150Magnetometer magnetometer) {
        if (magnetometer == null) {
            return;
        }
        this.magnetometer = magnetometer;
        magnetometerHandler = new MagnetometerHandler(board, magnetometerMessageHandler);
        try {
            this.magnetometer.routeData()
                    .fromBField()
                    .stream(MetaWearStreamingService.MAGNETOMETER_STREAM_KEY)
                    .commit()
                    .onComplete(magnetometerHandler);
        } catch (Exception e) {
            Trace.e(LOG_TAG + ": " + ExceptionUtils.getMessage(e) + " "
                    + ExceptionUtils.getStackTrace(e));
        }
    }

    public void stopAccelerometerStreaming() {
        if (accelerometerHandler == null) {
            return;
        }
        final int routeId = accelerometerHandler.getRouteId();
        if (routeId == -1) {
            return;
        }
        final RouteManager routeManager = board.getRouteManager(accelerometerHandler.getRouteId());
        if (routeManager != null) {
            final boolean unsubscribe = routeManager.unsubscribe(MetaWearStreamingService.ACCELEROMETER_STREAM_KEY);
            Trace.d(LOG_TAG + ": unsubscribe accelerometer " + (unsubscribe ? "success" : "not successful"));
        }
    }

    public void stopGyroscopeStreaming() {
        if (gyroscopeHandler == null) {
            return;
        }
        final int routeId = gyroscopeHandler.getRouteId();
        if (routeId == -1) {
            return;
        }
        final RouteManager routeManager = board.getRouteManager(gyroscopeHandler.getRouteId());
        if (routeManager != null) {
            final boolean unsubscribe = routeManager.unsubscribe(MetaWearStreamingService.GYROSCOPE_STREAM_KEY);
            Trace.d(LOG_TAG + ": unsubscribe gyroscope " + (unsubscribe ? "success" : "not successful"));
        }
    }

    public void stopMagnetometerStreaming() {
        if (magnetometerHandler == null) {
            return;
        }
        final int routeId = magnetometerHandler.getRouteId();
        if (routeId == -1) {
            return;
        }
        final RouteManager routeManager = board.getRouteManager(routeId);
        if (routeManager != null) {
            final boolean unsubscribe = routeManager.unsubscribe(MetaWearStreamingService.MAGNETOMETER_STREAM_KEY);
            Trace.d(LOG_TAG + ": unsubscribe magnetometer " + (unsubscribe ? "success" : "not successful"));
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        final BoardConnectionHandler that = (BoardConnectionHandler) o;

        return board.getMacAddress().equals(that.board.getMacAddress());
    }

    @Override
    public int hashCode() {
        return board.getMacAddress().hashCode();
    }

    public interface OnRecordInsertListener {
        void onRecordInserted(BoardReading reading);
    }

    private void onDataUpdate(final Calendar timestamp) {
        final BoardReading reading = new BoardReading();
        reading.setTimestamp(timestamp.getTime());
        reading.setBoardAddress(board.getMacAddress());
        // Hold local references because updates happen very quickly and asynchronously
        final ModuleReading ar = lastAccelerometerReading;
        final ModuleReading gr = lastGyroscopeReading;
        final ModuleReading mr = lastMagnetometerReading;
        if (ar != null) {
            reading.setAccX(ar.x);
            reading.setAccY(ar.y);
            reading.setAccZ(ar.z);
        }
        if (gr != null) {
            reading.setGyrX(gr.x);
            reading.setGyrY(gr.y);
            reading.setGyrZ(gr.z);
        }
        if (mr != null) {
            reading.setMagX(mr.x);
            reading.setMagY(mr.y);
            reading.setMagZ(mr.z);
        }

        if (listener != null) {
            listener.onNewDataRecord(board, reading);
        }
    }

    public void stop() {
        disconnectBoard();
    }

    public interface BoardConnectionListener {
        void onBoardConnected(MetaWearBoard board);
        void onBoardDisconnected(MetaWearBoard board);
        void onConnectionFailed(MetaWearBoard board);
        void onNewDataRecord(MetaWearBoard board, BoardReading reading);
    }

    private class ConnectionStateHandlerInternal extends MetaWearBoard.ConnectionStateHandler {
        @Override
        public void connected() {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " connected");
            if (listener != null) {
                listener.onBoardConnected(board);
            }
        }

        @Override
        public void disconnected() {
            Trace.d(LOG_TAG + ": " + board.getMacAddress() + " disconnected");
            if (listener != null) {
                listener.onBoardDisconnected(board);
            }
        }

        @Override
        public void failure(int status, final Throwable error) {
            Trace.e(LOG_TAG + ": " + board.getMacAddress() + " connection failed with error code \'"
                    + status + "\'. " + ExceptionUtils.getMessage(error));
            if (listener != null) {
                listener.onConnectionFailed(board);
            }
        }

    }

    private class AccelerometerMessageHandler implements RouteManager.MessageHandler {
        @Override
        public void process(Message message) {
            final CartesianFloat axes = message.getData(CartesianFloat.class);
            final Calendar timestamp = message.getTimestamp();
            lastAccelerometerReading = new ModuleReading(timestamp, axes.x(), axes.y(),
                    axes.z());
            onDataUpdate(timestamp);
        }
    }

    private class GyroscopeMessageHandler implements RouteManager.MessageHandler {
        @Override
        public void process(Message msg) {
            final CartesianFloat spinData = msg.getData(CartesianFloat.class);
            final Calendar timestamp = msg.getTimestamp();
            lastGyroscopeReading = new ModuleReading(timestamp, spinData.x(), spinData.y(),
                    spinData.z());
            onDataUpdate(timestamp);
        }
    }

    private class MagnetometerMessageHandler implements RouteManager.MessageHandler {
        @Override
        public void process(Message msg) {
            final CartesianFloat bField = msg.getData(CartesianFloat.class);
            final Calendar timestamp = msg.getTimestamp();
            lastMagnetometerReading = new ModuleReading(timestamp, bField.x(),
                    bField.y(), bField.z());
            onDataUpdate(timestamp);
        }
    }
}
