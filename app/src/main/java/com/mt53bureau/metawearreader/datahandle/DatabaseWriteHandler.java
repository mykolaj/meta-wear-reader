package com.mt53bureau.metawearreader.datahandle;

import android.content.Context;
import android.os.Handler;

import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.db.BoardReading;
import com.mt53bureau.metawearreader.db.CaptureSession;
import com.mt53bureau.metawearreader.db.ReadingRepository;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.LinkedList;
import java.util.Queue;


public class DatabaseWriteHandler {
    private static final String LOG_TAG = String.format("%s.%s", BuildConfig.LOG_TAG, DatabaseWriteHandler.class.getSimpleName());
    private final Context context;
    private final CaptureSession captureSession;
    private final Handler writerHandler;
    private final Queue<BoardReading> taskQueue = new LinkedList<>();
    private volatile boolean isIdle = true;
    private volatile boolean shouldStop;

    /**
     * @param context
     * @param captureSession
     * @param writerHandler  A Handler of a thread which should handle a database I/O operations
     */
    public DatabaseWriteHandler(Context context, final CaptureSession captureSession, Handler writerHandler) {
        this.context = context;
        this.captureSession = captureSession;
        if (writerHandler != null) {
            this.writerHandler = writerHandler;
        } else {
            this.writerHandler = new Handler();
        }
    }

    public void add(BoardReading reading) {
        if (shouldStop) {
            Trace.d(LOG_TAG + ": skip a record. Save processor already stopped");
            return;
        }
        reading.setSessionId(captureSession.getId());
        taskQueue.add(reading);
    }

    public void process(final BoardConnectionHandler.OnRecordInsertListener onRecordInsertListener) {
        if (!isIdle) {
            return;
        }

        isIdle = false;
        BoardReading reading;
        while ((reading = taskQueue.poll()) != null) {
            if (shouldStop) {
                break;
            }

            final BoardReading finalReading = reading;
            writerHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        ReadingRepository.insertReading(context, finalReading);
                        if (onRecordInsertListener != null) {
                            onRecordInsertListener.onRecordInserted(finalReading);
                        }
                    } catch (Exception e) {
                        Trace.e(LOG_TAG + ": " + ExceptionUtils.getMessage(e) + " "
                                + ExceptionUtils.getStackTrace(e));
                    }
                }
            });
        }

        isIdle = true;
    }

    public void stop() {
        shouldStop = true;
    }
}
