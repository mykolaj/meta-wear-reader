package com.mt53bureau.metawearreader.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;

import com.mbientlab.metawear.module.Bmi160Accelerometer;
import com.mbientlab.metawear.module.Bmi160Accelerometer.AccRange;
import com.mbientlab.metawear.module.Bmi160Gyro;
import com.mt53bureau.metawearreader.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

public final class PrefHelper {

    private static final @StringRes int pref_key_accel_acceleration_range = R.string.pref_key_acc_acceleration_range;
    private static final @StringRes int pref_key_accel_data_output_rate = R.string.pref_key_acc_data_output_rate;
    private static final @StringRes int pref_key_output_file_name_prefix = R.string.pref_key_file_name_prefix;
    private static final @StringRes int pref_key_gyro_output_data_rate = R.string.pref_key_gyro_output_data_rate;
    private static final @StringRes int pref_key_gyro_rotation_range = R.string.pref_key_gyro_rotation_range;
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("dd-MM-yyyy").withLocale(Locale.US);

    private PrefHelper() {
    }

    public static AccRange getAccelerometerAccelerationRange(Context context) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String defValue = context.getString(R.string.acc_acceleration_range_default);
        final String value = preferences.getString(
                context.getString(pref_key_accel_acceleration_range),
                defValue);
        if (value.equals(context.getString(R.string.acceleration_range_ar_2g))) {
            return AccRange.AR_2G;
        } else if (value.equals(context.getString(R.string.acceleration_range_ar_4g))) {
            return AccRange.AR_4G;
        } else if (value.equals(context.getString(R.string.acceleration_range_ar_8g))) {
            return AccRange.AR_8G;
        } else if (value.equals(context.getString(R.string.acceleration_range_ar_16g))) {
            return AccRange.AR_16G;
        } else {
            throw new IllegalStateException("Wrong preference value '" + value + "'");
        }
    }

    public static Bmi160Accelerometer.OutputDataRate getAccelerometerOutputDataRate(Context context) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String defaultRate = context.getString(R.string.acc_data_output_rate_default);
        final String value = preferences.getString(
                context.getString(pref_key_accel_data_output_rate),
                defaultRate);
        if (value.equals(context.getString(R.string.ac_or_12_5))) {
            return Bmi160Accelerometer.OutputDataRate.ODR_12_5_HZ;
        } else if (value.equals(context.getString(R.string.ac_or_25))) {
            return Bmi160Accelerometer.OutputDataRate.ODR_25_HZ;
        } else if (value.equals(context.getString(R.string.ac_or_50))) {
            return Bmi160Accelerometer.OutputDataRate.ODR_50_HZ;
        } else if (value.equals(context.getString(R.string.ac_or_100))) {
            return Bmi160Accelerometer.OutputDataRate.ODR_100_HZ;
        } else {
            throw new IllegalStateException("Wrong preference value '" + value + "'");
        }
    }

    public static String getOutputFileNamePrefix(Context context) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(context.getString(pref_key_output_file_name_prefix),
                DATE_FORMATTER.print(DateTime.now()));
    }

    public static float getGyroscopeOutputDataRate(Context context) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String defaultRate = context.getString(R.string.gyro_odr_default);
        final String value = preferences.getString(
                context.getString(pref_key_gyro_output_data_rate),
                defaultRate);
        if (value.equals(context.getString(R.string.gyro_odr25))) {
//            return Bmi160Gyro.OutputDataRate.ODR_25_HZ;
            return 25.0f;
        } else if (value.equals(context.getString(R.string.gyro_odr50))) {
//            return Bmi160Gyro.OutputDataRate.ODR_50_HZ;
            return 50.0f;
        } else if (value.equals(context.getString(R.string.gyro_odr100))) {
//            return Bmi160Gyro.OutputDataRate.ODR_100_HZ;
            return 100.0f;
        } else {
            throw new IllegalStateException("Wrong preference value '" + value + "'");
        }
    }


    public static Bmi160Gyro.FullScaleRange getGyroscopeFullScaleRange(final Context context) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String defValue = context.getString(R.string.gyro_rotation_range_default);
        final String value = preferences.getString(
                context.getString(pref_key_gyro_rotation_range),
                defValue);
        if (value.equals(context.getString(R.string.gyro_fsr125))) {
            return Bmi160Gyro.FullScaleRange.FSR_125;
        } else if (value.equals(context.getString(R.string.gyro_fsr250))) {
            return Bmi160Gyro.FullScaleRange.FSR_250;
        } else if (value.equals(context.getString(R.string.gyro_fsr500))) {
            return Bmi160Gyro.FullScaleRange.FSR_500;
        } else if (value.equals(context.getString(R.string.gyro_fsr1000))) {
            return Bmi160Gyro.FullScaleRange.FSR_1000;
        } else if (value.equals(context.getString(R.string.gyro_fsr2000))) {
            return Bmi160Gyro.FullScaleRange.FSR_2000;
        } else {
            throw new IllegalStateException("Wrong preference value '" + value + "'");
        }
    }
}
