package com.mt53bureau.metawearreader.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.mt53bureau.metawearreader.R;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.enums.ConnectionStatus;
import com.mt53bureau.metawearreader.events.BoardAddedToListEvent;
import com.mt53bureau.metawearreader.events.BoardConnectedEvent;
import com.mt53bureau.metawearreader.events.BoardDisconnectedEvent;
import com.mt53bureau.metawearreader.events.BoardRemovedFromList;
import com.mt53bureau.metawearreader.services.MetaWearStreamingService;
import com.mt53bureau.metawearreader.services.MetaWearStreamingService.BoardStateInfo;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * Activities that contain this fragment may implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConnectionStateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@SuppressWarnings("FieldCanBeLocal")
public class ConnectionStateFragment extends Fragment {
    private static final String TAG = ConnectionStateFragment.class.getSimpleName();
    public static final String SAVED_STATE_SLOT_INFO_LIST_KEY = "persisted_slot_info_list";
    private final ReaderServiceConnection readerServiceConnection = new ReaderServiceConnection();
    private final EventBusListener eventBusListener = new EventBusListener();
    private OnFragmentInteractionListener mListener;
    private MetaWearStreamingService readerService;
    private TextView textDeviceCount;
    private RecyclerView recyclerView;
    private ConnectionViewAdapter recyclerViewAdapter;

    public ConnectionStateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ConnectionStateFragment.
     */
    public static ConnectionStateFragment newInstance() {
        ConnectionStateFragment fragment = new ConnectionStateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerViewAdapter = new ConnectionViewAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connection_state, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        final Context context = getContext();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(SAVED_STATE_SLOT_INFO_LIST_KEY)) {
                ArrayList<SlotInfo> slotInfoList = savedInstanceState.getParcelableArrayList(SAVED_STATE_SLOT_INFO_LIST_KEY);
                if (slotInfoList != null) {
                    for (SlotInfo si : slotInfoList) {
                        final String address = si.getDeviceAddress();
                        recyclerViewAdapter.addSlot(address, si.getSlotNumber());
                        recyclerViewAdapter.updateSlot(address, si.isConnected);
                    }
                }
            }
        }

        EventBus eventBus = EventBus.getDefault();
        if (!eventBus.isRegistered(eventBusListener)) {
            eventBus.register(eventBusListener);
        }
        getContext().bindService(new Intent(getContext(), MetaWearStreamingService.class), readerServiceConnection, 0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus eventBus = EventBus.getDefault();
        if (eventBus.isRegistered(eventBusListener)) {
            eventBus.unregister(eventBusListener);
        }

        try {
            getContext().unbindService(readerServiceConnection);
        } catch (Exception e) {
            // Doesn't matter. Service unbound already
            Trace.w(TAG + ": " + ExceptionUtils.getMessage(e));
        }
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            Trace.d(TAG + ": " + context.toString() + " not implements OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        final ArrayList<SlotInfo> slotInfoList = Lists.newArrayList(recyclerViewAdapter.getSlotInfoList());
        if (!slotInfoList.isEmpty()) {
            outState.putParcelableArrayList(SAVED_STATE_SLOT_INFO_LIST_KEY, slotInfoList);
        } else {
            outState.remove(SAVED_STATE_SLOT_INFO_LIST_KEY);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnFragmentInteractionListener {
    }

    private static class ConnectionViewAdapter extends RecyclerView.Adapter<ConnectionViewAdapter.ViewHolder> {
        private final Map<Integer, SlotInfo> slotToInfoMapping = new TreeMap<>(new Comparator<Integer>() {
            @Override
            public int compare(final Integer lhs, final Integer rhs) {
                return lhs.compareTo(rhs);
            }
        });

        public ConnectionViewAdapter() {
            for (int i = 1; i <= 10; i++) {
                SlotInfo inf = new SlotInfo(SlotInfo.PLACEHOLDER_DEVICE_ADDRESS);
                inf.setSlotNumber(i);
                slotToInfoMapping.put(i, inf);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_slot_entry, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            // There is no 0 slot in a mapping
            SlotInfo slotInfo = Iterables.get(slotToInfoMapping.values(), position);
            holder.textSlotDescription.setText(String.format(Locale.getDefault(), "%d",
                    slotInfo.getSlotNumber()));
            @DrawableRes int drawableRes;
            if (SlotInfo.PLACEHOLDER_DEVICE_ADDRESS.equals(slotInfo.getDeviceAddress())) {
                drawableRes = R.drawable.slot_indicator_inactive;
            } else {
                drawableRes = slotInfo.isConnected() ? R.drawable.slot_indicator_connected
                        : R.drawable.slot_indicator_disconnected;
            }
            holder.textSlotDescription.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, drawableRes);
        }

        @Override
        public int getItemCount() {
            return slotToInfoMapping.size();
        }

        private void updateSlotInternal(final String deviceAddress, final boolean isConnected, final boolean notifyDataSetChanged) {
            final SlotInfo slotInfo = Iterables.tryFind(slotToInfoMapping.values(), new Predicate<SlotInfo>() {
                @Override
                public boolean apply(final SlotInfo input) {
                    return deviceAddress.equals(input.getDeviceAddress());
                }
            }).orNull();
            if (slotInfo == null) {
                return;
            }
            slotInfo.setIsConnected(isConnected);
            if (notifyDataSetChanged) {
                notifyDataSetChanged();
            }
        }

        public void updateSlot(final String deviceAddress, final boolean isConnected) {
            updateSlotInternal(deviceAddress, isConnected, true);
        }

        public void addSlot(final String address, final int slot) {
            // Check if a slot is already present in the list
            final boolean contains = Iterables.contains(slotToInfoMapping.values(), new Predicate<SlotInfo>() {
                @Override
                public boolean apply(final SlotInfo input) {
                    return address.equals(input.getDeviceAddress());
                }
            });

            if (contains) {
                return;
            }

            SlotInfo inf = new SlotInfo(address);
            inf.setSlotNumber(slot);
            slotToInfoMapping.put(slot, inf);
            notifyDataSetChanged();
        }

        public void removeSlot(final String deviceAddress) {
            // Check if a slot is present in the list
            final int position = Iterables.indexOf(slotToInfoMapping.values(), new Predicate<SlotInfo>() {
                @Override
                public boolean apply(final SlotInfo input) {
                    return deviceAddress.equals(input.getDeviceAddress());
                }
            });

            if (position == -1) {
                return;
            }

            // Instead of removing a mapping entirely we replace it with a placeholder.
            // Plus 1 is needed to skip 0 slot number and start from 1
            final SlotInfo slotInfo = new SlotInfo(SlotInfo.PLACEHOLDER_DEVICE_ADDRESS);
            slotInfo.slotNumber = position + 1;
            slotToInfoMapping.put(position + 1, slotInfo);
            notifyDataSetChanged();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            private final TextView textSlotDescription;

            public ViewHolder(final View itemView) {
                super(itemView);
                textSlotDescription = (TextView) itemView.findViewById(R.id.slot_description);
            }

        }

        public Collection<SlotInfo> getSlotInfoList() {
            return new ArrayList<>(slotToInfoMapping.values());
        }
    }

    public static class SlotInfo implements Parcelable {
        public static final String PLACEHOLDER_DEVICE_ADDRESS = "00:00:00:00:00";
        private int slotNumber;
        private final String deviceAddress;
        private boolean isConnected;

        public SlotInfo(final String deviceAddress) {
            this.deviceAddress = deviceAddress;
        }

        public int getSlotNumber() {
            return slotNumber;
        }

        public void setSlotNumber(int slotNumber) {
            this.slotNumber = slotNumber;
        }

        public String getDeviceAddress() {
            return deviceAddress;
        }

        public boolean isConnected() {
            return isConnected;
        }

        public void setIsConnected(boolean isConnected) {
            this.isConnected = isConnected;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.slotNumber);
            dest.writeString(this.deviceAddress);
            dest.writeByte(this.isConnected ? (byte) 1 : (byte) 0);
        }

        protected SlotInfo(Parcel in) {
            this.slotNumber = in.readInt();
            this.deviceAddress = in.readString();
            this.isConnected = in.readByte() != 0;
        }

        public static final Parcelable.Creator<SlotInfo> CREATOR = new Parcelable.Creator<SlotInfo>() {
            @Override
            public SlotInfo createFromParcel(Parcel source) {
                return new SlotInfo(source);
            }

            @Override
            public SlotInfo[] newArray(int size) {
                return new SlotInfo[size];
            }
        };
    }

    private class ReaderServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            readerService = ((MetaWearStreamingService.MetaWearReaderServiceBinder) service).getService();
            if (isAdded()) {
                final Collection<BoardStateInfo> connectedBoardsInfo = readerService.getConnectedBoardsInfo();
                for (BoardStateInfo boardStateInfo : connectedBoardsInfo) {
                    final String address = boardStateInfo.getBoard().getMacAddress();
                    final ConnectionStatus connectionStatus = boardStateInfo.getConnectionStatus();
                    boolean isConnected = connectionStatus != null &&
                            connectionStatus == ConnectionStatus.CONNECTED;
                    recyclerViewAdapter.updateSlot(address, isConnected);
                }
            }
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            readerService = null;
        }

    }

    private class EventBusListener {

        @Subscribe
        public void onEventMainThread(BoardConnectedEvent event) {
            recyclerViewAdapter.updateSlot(event.getDeviceAddress(), true);
        }

        @Subscribe
        public void onEventMainThread(BoardDisconnectedEvent event) {
            recyclerViewAdapter.updateSlot(event.getDeviceAddress(), false);
        }

        @Subscribe
        public void onEventMainThread(BoardAddedToListEvent event) {
            recyclerViewAdapter.addSlot(event.getDeviceAddress(), event.getSlotNumber());
        }

        @Subscribe
        public void onEventMainThread(BoardRemovedFromList event) {
            recyclerViewAdapter.removeSlot(event.getDeviceAddress());
        }

    }
}
