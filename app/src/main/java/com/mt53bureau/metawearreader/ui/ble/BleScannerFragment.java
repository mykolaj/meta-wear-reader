package com.mt53bureau.metawearreader.ui.ble;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.collect.Lists;
import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.R;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.enums.ConnectionStatus;
import com.mt53bureau.metawearreader.events.BatteryUpdateEvent;
import com.mt53bureau.metawearreader.events.BoardAddedToListEvent;
import com.mt53bureau.metawearreader.events.ConnectionStateChangedReport;
import com.mt53bureau.metawearreader.events.DataRecordingStartedEvent;
import com.mt53bureau.metawearreader.events.DataRecordingStoppedEvent;
import com.mt53bureau.metawearreader.events.DeviceDiscoveredEvent;
import com.mt53bureau.metawearreader.events.DeviceScanFinishedEvent;
import com.mt53bureau.metawearreader.events.DeviceScanStartedEvent;
import com.mt53bureau.metawearreader.events.DeviceStateChangedReport;
import com.mt53bureau.metawearreader.model.ItemInfoWrapper;
import com.mt53bureau.metawearreader.model.ScannedDeviceInfo;
import com.mt53bureau.metawearreader.services.BleScannerService;
import com.mt53bureau.metawearreader.services.MetaWearStreamingService;
import com.mt53bureau.metawearreader.ui.decor.DividerItemDecoration;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("FieldCanBeLocal")
public class BleScannerFragment extends Fragment {

    public static final String TAG = BleScannerFragment.class.getSimpleName();
    private static final int REQUEST_ENABLE_BT = 11;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 12;

    private static final String LOG_TAG = BuildConfig.LOG_TAG + "." + BleScannerFragment.class.getSimpleName();
    public static final String SAVED_STATE_DEVICE_LIST_KEY = "persisted_device_list";

    private final EventBusListener mEventBusListener = new EventBusListener();
    private final ReaderServiceConnection readerServiceConnection = new ReaderServiceConnection();
    private final BleScannerServiceConnection bleScannerServiceConnection = new BleScannerServiceConnection();
    private DeviceInfoAdapter scannedDevicesAdapter;
    private Handler uiHandler;
    private BluetoothAdapter btAdapter;
    private boolean isScanReady;
    private ScannerFragmentHost mFragmentActionsListener;
    private RecyclerView.LayoutManager mRecyclerViewLayoutManager;
    private RecyclerView mRecyclerView;
    private MetaWearStreamingService readerService;
    private BleScannerService bleScannerService;

    public BleScannerFragment() {
        // Required empty public constructor
    }

    public static BleScannerFragment newInstance() {
        final BleScannerFragment fragment = new BleScannerFragment();
        final Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context context = getContext();
        scannedDevicesAdapter = new DeviceInfoAdapter(getContext(), new OnDeviceCheckStateListener());
        btAdapter = ((BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();

        if (btAdapter == null) {
            new AlertDialog.Builder(context).setTitle(R.string.dialog_title_error)
                    .setMessage(R.string.error_no_bluetooth_adapter)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if (mFragmentActionsListener != null) {
                                mFragmentActionsListener.onBluetoothNotSupported();
                            }
                        }
                    }).create().show();
        } else if (!btAdapter.isEnabled()) {
            final Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            isScanReady = true;
        }
        context.bindService(new Intent(context, BleScannerService.class),
                bleScannerServiceConnection, Context.BIND_AUTO_CREATE);
        BleScannerService.start(context);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bleScannerService != null) {
            if (!bleScannerService.isScanning()) {
                BleScannerService.stop(getContext());
            }
            try {
                getContext().unbindService(bleScannerServiceConnection);
            } catch (Exception e) {
                Trace.w(TAG + ": onDestroy: " + ExceptionUtils.getMessage(e) );
            }
        }
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        if (context instanceof ScannerFragmentHost) {
            mFragmentActionsListener = (ScannerFragmentHost) context;
        } else {
            throw new IllegalStateException(context.toString() + " must implement "
                    + ScannerFragmentHost.class.getSimpleName() + " interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentActionsListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_CANCELED) {
                    isScanReady = false;
                    if (mFragmentActionsListener != null) {
                        mFragmentActionsListener.onUserRefusedEnableBluetooth();
                    }
                } else {
                    isScanReady = true;
                    if (mFragmentActionsListener != null) {
                        mFragmentActionsListener.onBluetoothEnabled();
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        uiHandler = new Handler();
        final View view = inflater.inflate(R.layout.fragment_ble_scaner, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.device_list);
        mRecyclerView.setHasFixedSize(true);
        final DividerItemDecoration decor = new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL);
        // Do not draw divider after last view
        decor.setDrawDividerAfterLastItem(false);
        mRecyclerView.addItemDecoration(decor);

        mRecyclerViewLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mRecyclerViewLayoutManager);
        mRecyclerView.setAdapter(scannedDevicesAdapter);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        scannedDevicesAdapter.setOnAdapterActionsListener(new DeviceInfoAdapter.OnAdapterActionsListener() {
            @Override
            public void onScanButtonClick() {
                if (bleScannerService != null) {
                    bleScannerService.onScanButtonClicked();
                }
            }
        });

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(SAVED_STATE_DEVICE_LIST_KEY)) {
                final ArrayList<ItemInfoWrapper> deviceList = savedInstanceState.getParcelableArrayList(SAVED_STATE_DEVICE_LIST_KEY);
                scannedDevicesAdapter.update(deviceList);
            }
        }

        final EventBus eventBus = EventBus.getDefault();
        if (!eventBus.isRegistered(mEventBusListener)) {
            eventBus.register(mEventBusListener);
        }

        final Context context = getContext();
        context.bindService(new Intent(context, MetaWearStreamingService.class), readerServiceConnection, 0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        final EventBus eventBus = EventBus.getDefault();
        if (eventBus.isRegistered(mEventBusListener)) {
            eventBus.unregister(mEventBusListener);
        }
        try { getContext().unbindService(readerServiceConnection); } catch (Exception e) { /* OK */ }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        final ArrayList<ItemInfoWrapper> deviceList = Lists.newArrayList(scannedDevicesAdapter.getDeviceList());
        if (!deviceList.isEmpty()) {
            outState.putParcelableArrayList(SAVED_STATE_DEVICE_LIST_KEY, deviceList);
        } else {
            outState.remove(SAVED_STATE_DEVICE_LIST_KEY);
        }
    }

    /**
     * Starts scanning for Bluetooth LE devices
     */
    public void onStartBleScan() {
        if (!isAdded()) {
            return;
        }
        scannedDevicesAdapter.clear();
        updateScanButtonLabel(true);
    }

    private void updateAdapterWithNewDevice(final ScannedDeviceInfo info) {
        if (!isAdded()) {
            return;
        }
        scannedDevicesAdapter.update(info);
    }

    /**
     * Stops the Bluetooth LE scan
     */
    public void onStopBleScan() {
        if (!isAdded()) {
            return;
        }
        updateScanButtonLabel(false);
    }

    @TargetApi(23)
    private boolean checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Android M Permission check
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.title_request_permission);
            builder.setMessage(R.string.error_location_access);
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                }
            });
            builder.show();
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isScanReady = true;
                }
            }
        }
    }


    private void updateScanButtonLabel(final boolean isScanInProgress) {
        if (!isAdded()) {
            return;
        }
        if (scannedDevicesAdapter != null) {
            scannedDevicesAdapter.updateScanButton(isScanInProgress);
        }
    }


    public interface ScannerFragmentHost {

        void onDeviceChecked(String address, boolean isChecked);

        void onUserRefusedEnableBluetooth();

        void onBluetoothNotSupported();

        void onBluetoothEnabled();
    }

    private class BleScannerServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            bleScannerService = ((BleScannerService.BleScannerServiceBinder) service).getService();
            updateScanButtonLabel(bleScannerService.isScanning());
            final Collection<ScannedDeviceInfo> discoveredDevices = bleScannerService.getDiscoveredDevices();
            if (isAdded()) {
                for (ScannedDeviceInfo dev : discoveredDevices) {
                    scannedDevicesAdapter.update(dev);
                }
            }
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            bleScannerService = null;
        }
    }

    private class OnDeviceCheckStateListener implements DeviceInfoAdapter.OnDeviceCheckListener {
        @Override
        public void onDeviceChecked(final String address) {
            if (mFragmentActionsListener != null) {
                mFragmentActionsListener.onDeviceChecked(address, true);
            }
        }

        @Override
        public void onDeviceUnchecked(final String address) {
            if (mFragmentActionsListener != null) {
                mFragmentActionsListener.onDeviceChecked(address, false);
            }
        }
    }

    private class EventBusListener {

        @Subscribe
        public void onEventMainThread(DeviceStateChangedReport event) {
            if (!isAdded()) {
                return;
            }
            if (event instanceof ConnectionStateChangedReport) {
                ConnectionStatus st = ((ConnectionStateChangedReport) event).getConnectionStatus();
                scannedDevicesAdapter.updateConnectionStatus(event.getDeviceAddress(), st);

            } else if (event instanceof BatteryUpdateEvent) {
                final byte batteryLevel = ((BatteryUpdateEvent) event).getBatteryLevel();
                scannedDevicesAdapter.updateBattery(event.getDeviceAddress(), batteryLevel);
            }
        }

        @Subscribe
        public void onEventMainThread(final DeviceDiscoveredEvent event) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    updateAdapterWithNewDevice(event.getDeviceInfo());
                }
            });
        }

        @Subscribe
        public void onEventMainThread(DeviceScanFinishedEvent event) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    onStopBleScan();
                }
            });
        }

        @Subscribe
        public void onEventMainThread(DeviceScanStartedEvent event) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    onStartBleScan();
                }
            });
        }

        @Subscribe
        public void onEventMainThread(final BoardAddedToListEvent event) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (!isAdded()) {
                        return;
                    }
                    scannedDevicesAdapter.updateSlotNumber(event.getDeviceAddress(), event.getSlotNumber());
                }
            });
        }

        @Subscribe
        public void onEventMainThread(DataRecordingStartedEvent event) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    onDataRecordingStarted();
                }
            });
        }

        @Subscribe
        public void onEventMainThread(DataRecordingStoppedEvent event) {
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    onDataRecordingStopped();
                }
            });
        }
    }

    private void onDataRecordingStarted() {
        if (!isAdded()) {
            return;
        }

        if (scannedDevicesAdapter != null) {
            scannedDevicesAdapter.enableCheckboxInput(false);
            scannedDevicesAdapter.enableScanButtonInput(false);
        }
    }

    private void onDataRecordingStopped() {
        if (!isAdded()) {
            return;
        }

        if (scannedDevicesAdapter != null) {
            scannedDevicesAdapter.enableCheckboxInput(true);
            scannedDevicesAdapter.enableScanButtonInput(true);
        }
    }

    private class ReaderServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            readerService = ((MetaWearStreamingService.MetaWearReaderServiceBinder) service).getService();
            final Collection<String> selectedBoards = readerService.getSelectedBoards();
            if (scannedDevicesAdapter != null) {
                scannedDevicesAdapter.updateCheckedState(selectedBoards);
                if (readerService.isDataRecordingInProgress()) {
                    scannedDevicesAdapter.enableCheckboxInput(false);
                } else {
                    scannedDevicesAdapter.enableCheckboxInput(true);
                }
            }
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            readerService = null;
        }

    }


}
