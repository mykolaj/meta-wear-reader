package com.mt53bureau.metawearreader.ui;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.google.common.collect.Iterables;
import com.mt53bureau.metawearreader.BuildConfig;
import com.mt53bureau.metawearreader.R;
import com.mt53bureau.metawearreader.Trace;
import com.mt53bureau.metawearreader.services.MetaWearStreamingService;
import com.mt53bureau.metawearreader.settings.SettingsActivity;
import com.mt53bureau.metawearreader.ui.ble.BleScannerFragment;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("FieldCanBeLocal")
public class MainActivity extends AppCompatActivity implements BleScannerFragment.ScannerFragmentHost, SessionFileNameDialog.FileNameDialogHost {
    private static final String LOG_TAG = BuildConfig.LOG_TAG + "." + MainActivity.class.getSimpleName();
    private ReaderServiceConnection readerServiceConnection = new ReaderServiceConnection();
    private CoordinatorLayout coordinatorLayout;
    private SectionsPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;
    private MetaWearStreamingService readerService;
    private CompoundButton startRecordSwitch;
    private OnStartRecordSwitchListener onStartRecordSwitchListener;

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.root_view_coordinator);
        startRecordSwitch = (CompoundButton) findViewById(R.id.start_record_switch);
        onStartRecordSwitchListener = new OnStartRecordSwitchListener();
        startRecordSwitch.setOnCheckedChangeListener(onStartRecordSwitchListener);

        viewPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());

        // Set up the ViewPager
        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(viewPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        MetaWearStreamingService.start(this);
        bindService(new Intent(this, MetaWearStreamingService.class), readerServiceConnection, 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(readerServiceConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                SettingsActivity.start(this);
                return true;

            case R.id.action_start_bt_connect:
                if (readerService == null) {
                    return true;
                }
                if (readerService.isDataRecordingInProgress()) {
                    toggleDataReading(false, null);
                    setStartRecordSwitchChecked_NoTrigger(false);
                } else {
                    askFileName();
                }
                return true;

            case R.id.action_exit:
                MetaWearStreamingService.stop(this);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        if (readerService != null) {
            MenuItem item = menu.findItem(R.id.action_start_bt_connect);
            if (readerService.isDataRecordingInProgress()) {
                item.setTitle(R.string.action_bt_disconnect);
            } else {
                item.setTitle(R.string.action_bt_connect);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDeviceChecked(final String address, final boolean isChecked) {
        if (isChecked) {
            Trace.d(LOG_TAG + ": device checked \'" + address + "\'");
            readerService.selectDevice(address);
        } else {
            Trace.d(LOG_TAG + ": device unchecked \'" + address + "\'");
            readerService.unselectDevice(address);
        }
    }

    @Override
    public void onUserRefusedEnableBluetooth() {
        // TODO Show information dialog with OK button. Finish activity only when the button is clicked
        if (!BuildConfig.DEBUG) {
            finish();
        }
    }

    @Override
    public void onBluetoothNotSupported() {
        // TODO Show information dialog with OK button. Finish activity only when the button is clicked
        if (!BuildConfig.DEBUG) {
            finish();
        }
    }

    @Override
    public void onBluetoothEnabled() {

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        // Notify support-fragments about activity result.
        // It's crazy to figure out why this isn't done automatically.
        final List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment f : fragments) {
                f.onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     *
     * @param turnOn
     * @param fileName  // TODO This is a stupid and confusing function parameter. Refactor it somehow
     */
    private void toggleDataReading(boolean turnOn, @Nullable String fileName) {
        if (readerService == null) {
            return;
        }
        if (readerService.isDataRecordingInProgress()) {
            if (!turnOn) {
                readerService.stopDataRecording();
            }
        } else {
            if (turnOn) {
                readerService.startDataRecording(fileName);
            }
        }
        invalidateOptionsMenu();
    }

    private void askFileName() {
        final SessionFileNameDialog dialog = SessionFileNameDialog.newInstance();
        dialog.setCancelable(true);
        dialog.show(getSupportFragmentManager(), SessionFileNameDialog.TAG);
    }

    @Override
    public void onFileName(@NonNull final Dialog dialog, @NonNull final String fileName) {
        toggleDataReading(true, fileName);
        setStartRecordSwitchChecked_NoTrigger(true);
        dialog.dismiss();
    }

    @Override
    public void onDialogCancelled(@NonNull final Dialog dialog) {
        setStartRecordSwitchChecked_NoTrigger(false);
        dialog.dismiss();
    }

    private void setStartRecordSwitchChecked_NoTrigger(final boolean isChecked) {
        if (startRecordSwitch == null) {
            return;
        }
        startRecordSwitch.setOnCheckedChangeListener(null);
        startRecordSwitch.setChecked(isChecked);
        startRecordSwitch.setOnCheckedChangeListener(onStartRecordSwitchListener);
    }

    private static class SectionsPagerAdapter extends FragmentPagerAdapter {
        private final Set<Page> pageSet = Collections.unmodifiableSet(EnumSet.of(Page.SCANNER));
        private final Context context;
        private final Map<Integer, Fragment> fragmentMap = new HashMap<>();

        public SectionsPagerAdapter(Context context, FragmentManager fm) {
            super(fm);
            this.context = context;
        }

        @Override
        public Fragment getItem(int position) {
            if (Page.SCANNER.getPosition() == position) {
                return BleScannerFragment.newInstance();
            } else if (Page.CONNECTION_STATUS.getPosition() == position) {
                return ConnectionStateFragment.newInstance();
            } else {
                return new Fragment();
            }
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(final ViewGroup container, final int position, final Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public int getCount() {
            return pageSet.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return context.getString(Iterables.get(pageSet, position).getTitleRes());
        }

        public Fragment getFragmentAtPosition(int position) {
            return fragmentMap.get(position);
        }

        private enum Page {
            SCANNER(R.string.scanner_tab_title, 0),
            CONNECTION_STATUS(R.string.connection_state_tab_title, 1);

            @StringRes
            private final int titleRes;
            private final int position;

            Page(@StringRes int title, int position) {
                this.position = position;
                titleRes = title;
            }

            public final int getTitleRes() {
                return titleRes;
            }

            public final int getPosition() {
                return position;
            }
        }
    }

    private class ReaderServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            readerService = ((MetaWearStreamingService.MetaWearReaderServiceBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            readerService = null;
        }

    }

    private class OnStartRecordSwitchListener implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
            if (readerService == null) {
                return;
            }
            if (readerService.isDataRecordingInProgress()) {
                if (!isChecked) {
                    toggleDataReading(false, null);
                }
            } else {
                if (isChecked) {
                    askFileName();
                }
            }
        }
    }
}
