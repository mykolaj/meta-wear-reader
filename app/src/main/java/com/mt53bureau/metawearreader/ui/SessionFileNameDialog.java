package com.mt53bureau.metawearreader.ui;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.mt53bureau.metawearreader.R;

import java.util.regex.Pattern;


@SuppressWarnings("NullableProblems")
public class SessionFileNameDialog extends DialogFragment {

    public static final String ILLEGAL_FILE_NAME_REGEX = "[^a-zA-Z0-9.-]";
    private static final Pattern ILLEGAL_FILE_NAME_PATTERN = Pattern.compile(ILLEGAL_FILE_NAME_REGEX);
    public static final String TAG = SessionFileNameDialog.class.getSimpleName();

    private EditText editFileName;
    private Button btnCancel;
    private Button btnConfirm;
    private FileNameDialogHost dialogHost;
    private TextInputLayout fileNameInputLayout;
    private EditFileNameTextWatcher editFileNameTextWatcher;

    public static SessionFileNameDialog newInstance() {
        return new SessionFileNameDialog();
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.setTitle("Capture File Name");
        return dialog;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialog_session_file_name, container, false);
        editFileName = (EditText) view.findViewById(R.id.edit_file_name);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnConfirm = (Button) view.findViewById(R.id.btn_confirm);
        fileNameInputLayout = (TextInputLayout) view.findViewById(R.id.file_name_input_layout);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnCancel.setOnClickListener(new OnCancelClickListener());
        btnConfirm.setOnClickListener(new OnConfirmClickListener());
        editFileNameTextWatcher = new EditFileNameTextWatcher();
        editFileName.addTextChangedListener(editFileNameTextWatcher);
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        dialogHost = (FileNameDialogHost) activity;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        dialogHost = null;
    }

    private class OnConfirmClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            // Clear errors if any
            if (!TextUtils.isEmpty(fileNameInputLayout.getError())) {
                fileNameInputLayout.setError(null);
                fileNameInputLayout.setErrorEnabled(false);
            }

            String fileName = editFileName.getText().toString();
            if (TextUtils.isEmpty(fileName)) {
                fileNameInputLayout.setError("File name can not be empty");
                return;
            }
            if (ILLEGAL_FILE_NAME_PATTERN.matcher(fileName).find()) {
                fileName = fileName.replaceAll(ILLEGAL_FILE_NAME_REGEX, "_");
            }
            if (dialogHost != null) {
                dialogHost.onFileName(SessionFileNameDialog.this.getDialog(), fileName.trim());
            }
        }
    }

    private class OnCancelClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            if (dialogHost != null) {
                dialogHost.onDialogCancelled(SessionFileNameDialog.this.getDialog());
            }
        }
    }

    public interface FileNameDialogHost {
        void onFileName(@NonNull Dialog dialog, @NonNull String fileName);
        void onDialogCancelled(@NonNull Dialog dialog);
    }

    private class EditFileNameTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {

        }

        @Override
        public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
            final String text = s.toString();
            final String result;
            if (ILLEGAL_FILE_NAME_PATTERN.matcher(text).find()) {
                result = text.replaceAll(ILLEGAL_FILE_NAME_REGEX, "_");
                editFileName.removeTextChangedListener(editFileNameTextWatcher);
                editFileName.setText(result);
                editFileName.addTextChangedListener(editFileNameTextWatcher);
                editFileName.setSelection(result.length());
            }
        }

        @Override
        public void afterTextChanged(final Editable s) {

        }
    }
}
